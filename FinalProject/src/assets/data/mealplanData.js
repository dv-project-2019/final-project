export const mealpalnData = [
    {
        id: 0,
        date: null,
        time: [
            {
                name: 'Morning',
                itemsList: [
                    {
                        itemId: 1
                    },
                    {
                        itemId: 2
                    },
                    {
                        itemId: 3
                    }
                ]
            },
            {
                name: 'Afternoon',
                itemsList: [
                    {
                        itemId: 1
                    },
                    {
                        itemId: 2
                    }
                ]
            },
            {
                name: 'Evening',
                itemsList: []
            }
        ]
    }
]