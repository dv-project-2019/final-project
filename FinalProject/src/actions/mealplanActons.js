export const SET_MEALPLAN_ID = 'SET_MEALPLAN_ID';

export const setMealplanId = (mealplanId) => {
    return{
        type: SET_MEALPLAN_ID,
        mealplanId
    }
}