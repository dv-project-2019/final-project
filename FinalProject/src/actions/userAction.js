export const SET_TOKEN = "SET_TOKEN";
export const SET_USER_INFO = "SET_USER_INFO";

export const setToken = (token)  => {
    return {
        type: SET_TOKEN,
        token
    }
}

export const setUserInfo = (userInfo)  => {
    return {
        type: SET_USER_INFO,
        userInfo
    }
}