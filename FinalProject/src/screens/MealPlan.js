import React, { useState, useEffect } from 'react';
import { StyleSheet, View, Text } from 'react-native';
import MainHeader from '../components/MainHeader';
import Topic from '../components/mealplan/topic';
import MealPlanCard from '../components/mealplan/mealplanCard';
import { connect } from 'react-redux';
import { checkMealplanCreated, createMealplan, getMealplanTable } from '../api';
import { setMealplanId } from '../actions/mealplanActons';
import { useNavigation } from '@react-navigation/native';
import Loading from '../components/loading';

const MealPlan = ({ token, setMealplanId, mealplanId }) => {
    const [headerTopic] = useState('Meal plan');
    const navigation = useNavigation();
    const [mealplanTable, setMealplanTable] = useState({});
    const [isLoading, setIsLoading] = useState(false);

    const fetchMealplan = (mealplanId) => {
        setIsLoading(true);
        getMealplanTable(token, mealplanId)
            .then((res) => {
                setMealplanTable(res.data);
                setTimeout(() => {
                    setIsLoading(false);
                }, 1000);
            })
            .catch((err) => {
                console.log(err);
                setTimeout(() => {
                    setIsLoading(false);
                }, 1000);
            })
    }

    useEffect(() => {
        const unsubscribe = navigation.addListener('focus', () => {
            const currentDate = new Date().getTime();
            setIsLoading(true);
            checkMealplanCreated(token, currentDate)
                .then((res) => {
                    if (res.data.result) {
                        createMealplan(token)
                            .then((res) => {
                                setMealplanId(res.data.id);
                                fetchMealplan(res.data.id)
                            })
                            .catch((err) => {
                                console.log(err)
                            })
                    }
                })
                .catch((err) => {
                    console.log(err)
                })
        });
        return unsubscribe;

    }, [token, navigation]);


    useEffect(() => {
        const unsubscribe = navigation.addListener('focus', () => {
            if (mealplanId !== 0) {
                fetchMealplan(mealplanId);
            }
        });
        return unsubscribe;

    }, [mealplanId, navigation]);

    return (
        <View style={styles.container}>
            <MainHeader title={headerTopic} />
            {isLoading ?
                <Loading visible={isLoading} />
                :
                <>
                    <Topic />
                    <MealPlanCard mealplan={mealplanTable} />
                </>
            }

        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    }
})

const mapStateToProps = (state) => {
    return {
        token: state.user.token,
        mealplanId: state.mealplan.id
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        setMealplanId: (id) => dispatch(setMealplanId(id))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(MealPlan);