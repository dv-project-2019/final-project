import React, { useState } from 'react';
import { View, StyleSheet, ScrollView } from 'react-native';
import MainHeader from '../components/MainHeader';
import ItemImage from '../components/editItem/itemImage';
import ItemForm from '../components/editItem/itemForm';
import { useRoute } from '@react-navigation/native';

const EditItemInfo = () => {
    const [headerTopic] = useState('Edit Item');
    const route = useRoute();
    const [ itemData, setItemData ] = useState(route.params.itemInfo);

    return (
        <View style={styles.container}>
            <MainHeader title={headerTopic} />
            <ScrollView contentContainerStyle={styles.contentContainer}>
                <ItemImage 
                    itemId={itemData.id}
                    itemImg={itemData.image} 
                />
                <ItemForm 
                    itemInformation={itemData} 
                />
            </ScrollView>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    contentContainer: {
        flexGrow: 1
    }
})

export default EditItemInfo;