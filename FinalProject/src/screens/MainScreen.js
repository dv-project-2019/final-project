import React from 'react';
import { View, StyleSheet } from 'react-native';
import LogoImage from '../components/mainScreen/logoImage';
import OptionButtons from '../components/mainScreen/optionButtons';

const MainScreen = () => {
    return (
        <View style={styles.container}>
            <View style={styles.main}>
               <LogoImage />
               <OptionButtons />
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    main: {
        flex: 1,
        backgroundColor: '#3D55B6',
    }
})

export default MainScreen;