import React, { useState } from 'react';
import { View, StyleSheet } from 'react-native';
import MainHeader from '../components/MainHeader';
import MenuList from '../components/more/menulist';
import LogoutButton from '../components/more/logoutButton';
import ShortBio from '../components/more/shortBio';
import { connect } from 'react-redux';
import Loading from '../components/loading';

const MoreMenu = ({ userInfo }) => {
    const [headerTopic] = useState('More');
    const [isLoading, setIsLoading] = useState(false);

    return (
        <View style={styles.container}>
            {isLoading ?
                <Loading visible={isLoading} />
                :
                <>
                    <MainHeader title={headerTopic} />
                    <ShortBio user={userInfo} />
                    <MenuList />
                    <LogoutButton setIsLoading={setIsLoading} />
                </>
            }

        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    }
})

const mapStateToProps = (state) => {
    return {
        userInfo: state.user.userInfo
    }
}

export default connect(mapStateToProps, null)(MoreMenu);