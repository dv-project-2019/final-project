import React, { useState, useEffect } from 'react';
import { View, StyleSheet, Alert } from 'react-native';
import MainHeader from '../components/MainHeader';
import SearchBar from '../components/pantry/searchBar';
import SelectCategory from '../components/pantry/selectCategory';
import ItemList from '../components/pantry/itemList';
import { connect } from 'react-redux';
import moment from 'moment';
import { getItemsList, updateItemExpired, getUserProfile } from '../api';
import { useNavigation } from '@react-navigation/native';
import Loading from '../components/loading';
import { setUserInfo } from '../actions/userAction';

const Pantry = ({ token, setUserInfo }) => {
    const [headerTopic] = useState('Pantry');
    const [searchValue, setSearchValue] = useState('');
    const [categorySelectedValue, setcategorySelectedValue] = useState('All');
    const [itemsList, setItemsList] = useState([]);
    const navigation = useNavigation();
    const [isLoading, setIsLoading] = useState(false);

    const fetchItemsList = () => {
        getItemsList(token)
            .then((res) => {
                setItemsList(res.data);
                setTimeout(() => {
                    setIsLoading(false);
                }, 2000);
            })
            .catch((err) => {
                console.log(err);
                setTimeout(() => {
                    setIsLoading(false);
                    Alert.alert("Error", "Cannot connect internet, please try again")
                }, 1000);
            })
    }

    useEffect(() => {
        if (token) {
            getUserProfile(token)
            .then((res) => {
                setUserInfo(res.data);
            })
            .catch((err) => {
                console.log(err);
            })
        }

    }, [token]);

    useEffect(() => {
        const unsubscribe = navigation.addListener('focus', () => {
            if (token) {
                setIsLoading(true);
                fetchItemsList();
            }
        });
        return unsubscribe;

    }, [token, navigation]);

    const setItemExpired = (itemId, expiredValue) => {
        updateItemExpired(itemId, expiredValue, token)
            .then(() => {
                fetchItemsList();
            })
            .catch((err) => {
                console.log(err)
            })
    }

    const checkExpiredItem = (items) => {
        let currentDate = moment().format("DD");
        let currentMonth = moment().format("MM");
        let currentYear = moment().format("YYYY");

        if (items !== undefined && items !== null) {
            items.map((item) => {
                if (!item.isExpired) {
                    let expirationDate = moment(item.expiration_date).format("DD");
                    let expirationMonth = moment(item.expiration_date).format("MM");
                    let expirationYear = moment(item.expiration_date).format("YYYY");

                    if (expirationYear < currentYear) {
                        return setItemExpired(item.id, item.isExpired);
                    } else if (expirationYear === currentYear) {
                        if (expirationMonth < currentMonth) {
                            return setItemExpired(item.id, item.isExpired);
                        } else if (expirationMonth === currentMonth) {
                            if (expirationDate <= currentDate) {
                                return setItemExpired(item.id, item.isExpired);
                            }
                        } else {
                            return item;
                        }

                    } else {
                        return item;
                    }
                } else {
                    let expirationDate = moment(item.expiration_date).format("DD");
                    let expirationMonth = moment(item.expiration_date).format("MM");
                    let expirationYear = moment(item.expiration_date).format("YYYY");

                    if (expirationYear > currentYear) {
                        return setItemExpired(item.id, item.isExpired);
                    } else if (expirationYear === currentYear) {
                        if (expirationMonth > currentMonth) {
                            return setItemExpired(item.id, item.isExpired);
                        } else if (expirationMonth === currentMonth) {
                            if (expirationDate > currentDate) {
                                return setItemExpired(item.id, item.isExpired);
                            }
                        } else {
                            return item;
                        }

                    } else {
                        return item;
                    }
                }
            })
        } 
        return items;
    }

    return (
        <View style={styles.container}>
            <MainHeader title={headerTopic} />
            <SearchBar
                searchValue={searchValue}
                setSearchValue={setSearchValue}
            />
            <SelectCategory
                selectedValue={categorySelectedValue}
                setSelectedValue={setcategorySelectedValue}
            />
            {isLoading ?
                <Loading visible={isLoading}/>
                :
                <ItemList
                    title={headerTopic}
                    itemsData={checkExpiredItem(itemsList)}
                    categoryValue={categorySelectedValue}
                    searchValue={searchValue}
                    token={token}
                    refreshData={fetchItemsList}
                />
            }
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    }
})

const mapStateToProps = (state) => {
    return {
        token: state.user.token
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        setUserInfo: (userInfo) => dispatch(setUserInfo(userInfo))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Pantry);