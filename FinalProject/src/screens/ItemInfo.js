import React, { useState, useEffect } from 'react';
import { View, StyleSheet, ScrollView } from 'react-native';
import MainHeader from '../components/MainHeader';
import ItemImage from '../components/itemInfo/itemImage';
import ItemDescription from '../components/itemInfo/itemDescript';
import { useRoute, useNavigation } from '@react-navigation/native';
import { connect } from 'react-redux';
import { getItemInfo } from '../api';
import Loading from '../components/loading';

const ItemInfo = ({ token }) => {
    const [headerTopic] = useState('Item Information');
    const route = useRoute();
    const [itemId] = useState(route.params.itemId);
    const [itemInfo, setItemInfo] = useState({});
    const navigation = useNavigation();
    const [isLoading, setIsLoading] = useState(false);

    useEffect(() => {
        setIsLoading(true);
        const unsubscribe = navigation.addListener('focus', () => {
            getItemInfo(itemId, token)
                .then((res) => {
                    setItemInfo(res.data);
                    setIsLoading(false);
                })
                .catch((err) => {
                    console.log(err);
                    setIsLoading(false);
                })
        });

        return unsubscribe;

    }, [navigation, itemId])

    return (
        <View style={styles.container}>
            <MainHeader
                title={headerTopic}
                itemData={itemInfo}
            />
            {isLoading ?
                <Loading visible={isLoading} />
                :
                <ScrollView contentContainerStyle={styles.contentContainer}>
                    <ItemImage item={itemInfo} />
                    <ItemDescription itemInformation={itemInfo} />
                </ScrollView>
            }


        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    contentContainer: {
        flexGrow: 1
    }
})

const mapStateToProps = (state) => {
    return {
        token: state.user.token
    }
}

export default connect(mapStateToProps, null)(ItemInfo);