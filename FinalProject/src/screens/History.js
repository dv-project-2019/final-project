import React, { useState, useEffect } from 'react';
import { StyleSheet, View } from 'react-native';
import MainHeader from '../components/MainHeader';
import HistoryList from '../components/history/histortList';
import { connect } from 'react-redux';
import moment from 'moment';
import { getHistoryList } from '../api';
import { useNavigation } from '@react-navigation/native';
import Loading from '../components/loading';

const History = ({ token }) => {
    const [headerTopic] = useState('History');
    const [historyList, setHistoryList] = useState([]);
    const [historyDateList, setHistoryDateList] = useState([]);
    const navigation = useNavigation();
    const [isLoading, setIsLoading] = useState(false);

    const fetchHistoryData = () => {
        getHistoryList(token)
            .then((res) => {
                setHistoryList(res.data);
                findHistoryDateList(res.data);
                setTimeout(() => {
                    setIsLoading(false);
                }, 1000);
               
            })
            .catch((err) => {
                console.log(err);
                setTimeout(() => {
                    setIsLoading(false);
                }, 500);
            })

    }

    const findHistoryDateList = (historyList) => {
        let historyDateList = [];
        historyList.map((history) => {
            if (!historyDateList.includes(moment(history.CreatedAt).format("D/MM/YYYY"))) {
                historyDateList.push(moment(history.CreatedAt).format("D/MM/YYYY"));
            }
        })
        let sortDate = historyDateList.sort((a, b) => b - a).reverse();
        setHistoryDateList(sortDate);
    }

    useEffect(() => {
        const unsubscribe = navigation.addListener('focus', () => {
            setIsLoading(true);
            fetchHistoryData();
        });
        return unsubscribe;

    }, []);

    return (
        <View style={styles.container}>
            <MainHeader title={headerTopic} />
            {isLoading ?
                <Loading visible={isLoading} />
                :
                <HistoryList
                    title={headerTopic}
                    historyDateList={historyDateList}
                    allHistory={historyList}
                />
            }

        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    }
})

const mapStateToProps = (state) => {
    return {
        token: state.user.token
    }
}

export default connect(mapStateToProps, null)(History);