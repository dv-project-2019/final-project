import React, { useState } from 'react';
import { View, StyleSheet } from 'react-native';
import AuthHeader from '../components/AuthHeader';
import LogoImage from '../components/login/LogoImage';
import LoginForm from '../components/login/logoForm';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

const Login = () => {
    const [screen] = useState('Login');

    return (
        <View style={styles.container}>
            <AuthHeader screen={screen} />
            <View style={styles.main}>
                <KeyboardAwareScrollView style={styles.KeyboardScrollViewStyle}>
                    <LogoImage />
                    <LoginForm />
                </KeyboardAwareScrollView>
            </View>


        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    main: {
        flex: 1,
        backgroundColor: '#3D55B6',
    },
    KeyboardScrollViewStyle: {
        flex: 1
    }
})

export default Login;