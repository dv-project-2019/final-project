import React, { useState, useEffect } from 'react';
import { View, StyleSheet, Alert } from 'react-native';
import MainHeader from '../components/MainHeader';
import ItemForm from '../components/addItem/itemForm';
import TopicImage from '../components/addItem/topicImage';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { addNewItem, uploadImage } from '../api';
import { connect } from 'react-redux';
import { useNavigation } from '@react-navigation/native';

const AddItem = ({ token }) => {
    const [headerTopic] = useState('Add Item');
    const [itemInfo, setItemInfo] = useState({
        category: "Fridge",
        expiration_date: new Date()
    });
    const [image, setImage] = useState("");
    const navigation = useNavigation();
    const [isLoading, setIsLoading] = useState(false);

    const addedSuccess = () => {
        setTimeout(() => {
            navigation.navigate('Pantry');
            setIsLoading(false);
            setItemInfo({
                category: "Fridge",
                expiration_date: new Date()
            });
            setImage("");
        }, 500);
    }

    const submitForm = (formvalue) => {
        addNewItem(formvalue, itemInfo, token)
            .then((res) => {
                const id = res.data.id
                if (image === "") {
                    addedSuccess();
                } else {
                    uploadImage(id, image, token)
                        .then(() => {
                            addedSuccess();
                        })
                        .catch((err) => {
                            console.log(err);
                            navigation.navigate('Pantry');
                            Alert.alert("Error", "Add item image is not success")
                        })
                }
            })
            .catch((err) => {
                setIsLoading(false);
                console.log(err);
                Alert.alert("Error", "Add item is not success, please try again")
            })
    }

    useEffect(() => {
        const unsubscribe = navigation.addListener('focus', () => {
            setItemInfo({
                category: "Fridge",
                expiration_date: new Date()
            });
            setImage("");
        });
        return unsubscribe;

    }, [navigation]);

    return (
        <View style={styles.container}>
            <MainHeader title={headerTopic} />
            <KeyboardAwareScrollView style={styles.KeyboardScrollViewStyle}>
                <TopicImage />
                <ItemForm
                    itemInfo={itemInfo}
                    setItemInfo={setItemInfo}
                    itemImage={image}
                    setItemImage={setImage}
                    onClickSubmit={submitForm}
                    isLoading={isLoading}
                />
            </KeyboardAwareScrollView>
        </View>

    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    KeyboardScrollViewStyle: {
        flex: 1
    }
})

const mapStateToProps = (state) => {
    return {
        token: state.user.token
    }
}

export default connect(mapStateToProps, null)(AddItem);