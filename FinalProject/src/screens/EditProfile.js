import React, { useState, useEffect } from 'react';
import { View, StyleSheet, ScrollView, Alert } from 'react-native';
import MainHeader from '../components/MainHeader';
import ProfileImage from '../components/editProfile/profileImage';
import ProfileForm from '../components/editProfile/profileForm';
import { connect } from 'react-redux';
import { updateUserProfile, getUserProfile } from '../api';
import { setUserInfo } from '../actions/userAction';
import { useNavigation } from '@react-navigation/native';

const EditProfile = ({ userInfo, token, setUserInfo }) => {
    const [headerTopic] = useState('Edit Profile');
    const [isLoading, setIsLoading] = useState(false);
    const navigation = useNavigation();
    const [profileInfo, setProfileInfo] = useState({
        email: userInfo.email,
        firstname: userInfo.firstname,
        lastname: userInfo.lastname,
        dob: new Date(userInfo.dob).getTime()
    });
    const [ userImage, setUserImage ] = useState("");

    const successAlert = () => {
        setTimeout(() => {
            Alert.alert("Success", "Update profile success");
            setIsLoading(false);
            navigation.navigate('Profile');
        }, 1000);
    }

    const errorAlert = () => {
        setTimeout(() => {
            Alert.alert("Error", "Update profile is not success, please try again");
            setIsLoading(false);
        }, 1000);
    }

    const updateProfile = (formValue) => {
        updateUserProfile(formValue, profileInfo, token)
            .then((res) => {
                setUserInfo(res.data);
                successAlert();
            })
            .catch((err) => {
                console.log(err);
                errorAlert();
            })
    }

    useEffect(() => {
        const unsubscribe = navigation.addListener('focus', () => {
            if (token) {
                getUserProfile(token)
                .then((res) => {
                    setUserInfo(res.data);
                })
                .catch((err) => {
                    console.log(err);
                })
            }
        });
        return unsubscribe;

    }, [token, navigation]);

    return (
        <View style={styles.container}>
            <MainHeader title={headerTopic} />
            <ScrollView contentContainerStyle={styles.contentContainer}>
                <ProfileImage 
                userImage={userImage} 
                setUserImage={setUserImage}
                userInfo={userInfo}
                token={token}
             />
                <ProfileForm
                    profileInfo={profileInfo}
                    setProfileInfo={setProfileInfo}
                    updateProfile={updateProfile}
                    isLoading={isLoading}
                    setIsLoading={setIsLoading}
                />
            </ScrollView>

        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    contentContainer: {
        flexGrow: 1
    }
})

const mapStateToProps = (state) => {
    return {
        userInfo: state.user.userInfo,
        token: state.user.token
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        setUserInfo: (userinfo) => dispatch(setUserInfo(userinfo))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(EditProfile);