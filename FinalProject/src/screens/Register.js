import React, { useState } from 'react';
import { View, StyleSheet, ScrollView, Alert } from 'react-native';
import AuthHeader from '../components/AuthHeader';
import RegisterForm from '../components/register/registerForm';
import { registerNewUser, checkEmailAvailability } from '../api/index';
import { useNavigation } from '@react-navigation/native';

const Register = () => {
    const [screen] = useState('Sign Up');
    const [dateOfBirth, setdateOfBirth] = useState(new Date().getTime());
    const navigation = useNavigation();
    const [isLoading, setIsLoading] = useState(false);

    const cleanTextInput = () => {
        setdateOfBirth(new Date().getTime());
    }
    
    const register = (formValue) => {
        checkEmailAvailability(formValue.email)
            .then((res) => {
                if (res.data.result) {
                    registerNewUser(formValue, dateOfBirth)
                        .then(() => {
                            Alert.alert("Success", "Registration success");
                            setTimeout(() => {
                                setIsLoading(false);
                                cleanTextInput();
                                navigation.navigate('Login');
                            }, 500);
                        })
                        .catch((err) => {
                            console.log(err);
                            setTimeout(() => {
                                setIsLoading(false);
                                cleanTextInput();
                                Alert.alert("Error", "Registration is not success, please try again");
                                navigation.navigate('MainScreen');
                            }, 1000);
                        })
                } else {
                    setTimeout(() => {
                    Alert.alert("Warning", "Email " + formValue.email  + " is already used");
                    }, 1000);
                }
            })
            .catch((err) => {
                console.log(err);
                setTimeout(() => {
                    setIsLoading(false);
                    Alert.alert("Error", "Cannot connect to the internet, please try again");
                    cleanTextInput();
                }, 1000);
            })
    }

    return (
        <View style={styles.container}>
            <AuthHeader screen={screen} />
            <ScrollView contentContainerStyle={styles.contentContainer}>
                <RegisterForm
                    dateOfBirth={dateOfBirth}
                    setdateOfBirth={setdateOfBirth}
                    onClickRegister={register}
                    isLoading={isLoading}
                    setIsLoading={setIsLoading}
                />
            </ScrollView>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    contentContainer: {
        flexGrow: 1
    },
    main: {
        flex: 1,
        backgroundColor: '#3D55B6',
    },
})

export default Register;