import React, { useState, useEffect } from 'react';
import { StyleSheet, View, Alert } from 'react-native';
import MainHeader from '../components/MainHeader';
import Topic from '../components/addMealplanSchedule/topic';
import ItemList from '../components/addMealplanSchedule/itemList';
import { useRoute, useNavigation } from '@react-navigation/native';
import { connect } from 'react-redux';
import Loading from '../components/loading';
import { getItemsList } from '../api';

const MealPlanSchedule = ({ token }) => {
    const [headerTopic] = useState('Add to schedule');
    const route = useRoute();
    const [timeName] = useState(route.params.time);
    const [mealplanId] = useState(route.params.mealplanId);
    const [timeId] = useState(route.params.timeId);
    const [isLoading, setIsLoading] = useState(false);
    const [itemsList, setItemsList] = useState([]);
    const navigation = useNavigation();

    const fetchItemsList = () => {
        getItemsList(token)
            .then((res) => {
                setItemsList(res.data);
                setTimeout(() => {
                    setIsLoading(false);
                }, 2000);
            })
            .catch((err) => {
                console.log(err);
                setTimeout(() => {
                    setIsLoading(false);
                }, 2000);
            })
    }

    useEffect(() => {
        const unsubscribe = navigation.addListener('focus', () => {
            if (token) {
                setIsLoading(true);
                fetchItemsList();
            } else {
                setIsLoading(true);
                setTimeout(() => {
                    setIsLoading(false);
                    Alert.alert("Error", "Network error, please try again")
                }, 1000);
            }
        });
        return unsubscribe;

    }, [token, navigation]);

    return (
        <View style={styles.container}>
            <MainHeader title={headerTopic} />
            {isLoading ?
                <Loading visible={isLoading} />
                :
                <>
                    <Topic time={timeName} />
                    <ItemList
                        title={headerTopic}
                        itemsList={itemsList}
                        mealplanId={mealplanId}
                        time={timeName}
                        timeId={timeId}
                        refreshItem={fetchItemsList}
                    />
                </>
            }

        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    }
})


const mapStateToProps = (state) => {
    return {
        token: state.user.token
    }
}

export default connect(mapStateToProps, null)(MealPlanSchedule);