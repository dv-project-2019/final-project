import axios from 'axios';

export const host = "https://eatontimeapi.jumpingcrab.com";

export function login(loginInfo) {
    return axios.post(`${host}/auth/login`, { email: loginInfo.email, password: loginInfo.password })
}

export function getItemsList(token) {
    return axios.get(`${host}/items`, {
        headers: {
            Authorization: `Bearer ${token}`
        }
    });
}

export function updateItemExpired(itemId, expiredValue, token) {
    return axios.patch(`${host}/item/${itemId}`, { isExpired: expiredValue }, {
        headers: {
            Authorization: `Bearer ${token}`
        }
    });
}

export function getItemInfo(itemId, token) {
    return axios.get(`${host}/item/${itemId}`, {
        headers: {
            Authorization: `Bearer ${token}`
        }
    });
}

export function updateItemInfo(formValue, itemInfo, itemId, token) {
    let itemName = formValue.itemName;
    let description = formValue.description;
    let calorie = parseFloat(formValue.calorie);
    let amount = parseFloat(formValue.amount);
    let unit = formValue.unit;
    let category = itemInfo.category;
    let expiration_date = new Date(itemInfo.expiration_date).getTime();

    return axios.patch(`${host}/item/${itemId}/update`, {
        itemName: itemName,
        description: description,
        calorie: calorie,
        amount: amount,
        unit: unit,
        category: category,
        expiration_date: expiration_date
    }, {
        headers: {
            Authorization: `Bearer ${token}`
        }
    });
}

export function removeItem(itemId, token) {
    return axios.delete(`${host}/item/${itemId}`, {
        headers: {
            Authorization: `Bearer ${token}`
        }
    });
}

export function getHistoryList(token) {
    return axios.get(`${host}/history`, {
        headers: {
            Authorization: `Bearer ${token}`
        }
    });
}

export function addNewItem(form, itemInfo, token) {
    let itemName = form.itemName;
    let description = form.description;
    let calorie = parseFloat(form.calorie);
    let amount = parseFloat(form.amount);
    let unit = form.unit;
    let category = itemInfo.category;
    let expiration_date = new Date(itemInfo.expiration_date).getTime();

    return axios.post(`${host}/item`, {
        itemName: itemName,
        description: description,
        calorie: calorie,
        amount: amount,
        unit: unit,
        category: category,
        expiration_date: expiration_date
    }, {
        headers: {
            Authorization: `Bearer ${token}`
        }
    });
}

export function uploadImage(itemId, image, token) {
    const form = new FormData();
    form.append("image", {
        uri: image.uri,
        name: image.fileName,
        type: image.type,
    })
    
    return axios.post(`${host}/item/${itemId}/image`, form, {
        headers: {
            Authorization: `Bearer ${token}`,
            "Content-Type": 'multipart/form-data'
        }
    });
}

export function getUserProfile (token){
    return axios.get(`${host}/user/profile`, {
        headers: {
            Authorization: `Bearer ${token}`
        }
    });
}

export function checkMealplanCreated(token, time){
    return axios.get(`${host}/checkMealplanCreated?time=${time}`, {
        headers: {
            Authorization: `Bearer ${token}`
        }
    });
}

export function createMealplan(token){
    return axios.post(`${host}/mealplan`, {}, {
        headers: {
            Authorization: `Bearer ${token}`
        }
    });
}

export function getMealplanTable(token, id){
    return axios.get(`${host}/mealplan/${id}`, {
        headers: {
            Authorization: `Bearer ${token}`
        }
    });
}

export function addItemToMoningplan(token, mealplanId, itemId, timeId){
    return axios.post(`${host}/mealplan/${mealplanId}/morning?timeId=${timeId}&itemId=${itemId}`, {}, {
        headers: {
            Authorization: `Bearer ${token}`
        }
    });
}

export function addItemToAfternoonplan(token, mealplanId, itemId, timeId){
    return axios.post(`${host}/mealplan/${mealplanId}/afternoon?timeId=${timeId}&itemId=${itemId}`, {}, {
        headers: {
            Authorization: `Bearer ${token}`
        }
    });
}

export function addItemToEveningplan(token, mealplanId, itemId, timeId){
    return axios.post(`${host}/mealplan/${mealplanId}/evening?timeId=${timeId}&itemId=${itemId}`, {}, {
        headers: {
            Authorization: `Bearer ${token}`
        }
    });
}

export function logout(token){
    return axios.post(`${host}/user/logout`, {}, {
        headers: {
            Authorization: `Bearer ${token}`
        }
    });
}

export function checkEmailAvailability(email) {
    return axios.get(`${host}/auth/checkEmailAvailability?email=${email}`)
}

export function updateUserProfile( form, userInfo, token ) {
    let dob = new Date(userInfo.dob).getTime();

    return axios.patch(`${host}/user/profile`, { email: userInfo.email, firstname: form.firstname, lastname: form.lastname, dob: dob}, {
        headers: {
            Authorization: `Bearer ${token}`
        }
    })
}

export  function uploadUserImage(image, token) {
    const form = new FormData();
    form.append("userImage", {
        uri: image.uri,
        name: image.fileName,
        type: image.type,
    })
    
    return axios.patch(`${host}/user/uploadimage`, form, {
        headers: {
            Authorization: `Bearer ${token}`,
            "Content-Type": 'multipart/form-data'
        }
    });
}

export function registerNewUser( form, dob ) {
    return axios.post(`${host}/auth/signup`, { email: form.email, firstname: form.firstname, lastname: form.lastname, dob: dob, password: form.password})
}