import React from 'react';
import { View, StyleSheet, Text } from 'react-native';
import { Divider } from 'react-native-paper';
import { dateFormat } from '../helper/dateFormat';

const ItemDescription = ({ itemInformation }) => {
    
    const checkItemDescription = (descript) => {
        if(descript !== '' && descript !== null) {
            return descript;
        }else{
            return "-"
        }
    }

    return (
        <View style={styles.itemInfoSection}>
            <View style={styles.itemData}>
                <View style={styles.itemName}>
                    <Text style={styles.itemNameText}>{itemInformation.itemName}</Text>
                </View>
               
                <View style={styles.itemInfoBlock}>
                    <View style={styles.itemTopic}>
                        <Text style={styles.itemTopicText}>Item Description</Text>
                    </View>
                    <View style={styles.itemDescript}>
                        <Text style={styles.itemDescriptText}>{checkItemDescription(itemInformation.description)}</Text>
                    </View>
                </View>
                <Divider />
                <View style={styles.itemInfoBlock}>
                    <View style={styles.itemTopic}>
                        <Text style={styles.itemTopicText}>Calories</Text>
                    </View>
                    <View style={styles.itemDescript}>
                        <Text style={styles.itemDescriptText}>{itemInformation.calorie} Calories </Text>
                    </View>
                </View>
                <Divider />
                <View style={styles.itemInfoBlock}>
                    <View style={styles.itemTopic}>
                        <Text style={styles.itemTopicText}>Amount</Text>
                    </View>
                    <View style={styles.itemDescript}>
                        <Text style={styles.itemDescriptText}>{itemInformation.amount} {itemInformation.unit} </Text>
                    </View>
                </View>
                <Divider />
                <View style={styles.itemInfoBlock}>
                    <View style={styles.itemTopic}>
                        <Text style={styles.itemTopicText}>Location</Text>
                    </View>
                    <View style={styles.itemDescript}>
                        <Text style={styles.itemDescriptText}>{itemInformation.category} </Text>
                    </View>
                </View>
                <Divider />
                <View style={styles.itemInfoBlock}>
                    <View style={styles.itemTopic}>
                        <Text style={styles.itemTopicText}>Expiration Date</Text>
                    </View>
                    <View style={styles.itemDescript}>
                        <Text style={styles.itemDescriptText}>{dateFormat(itemInformation.expiration_date)} </Text>
                    </View>
                </View>

            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    itemInfoSection: {
        flex: 1,
        backgroundColor: '#F9F9F9'
    },
    itemData: {
        flex: 1,
        marginLeft: 25,
        marginRight: 25,
        marginBottom: 10,
        marginTop: 10,
    },
    itemName: {
        flex: 0,
        paddingTop: 8,
        paddingBottom: 8,
        justifyContent: 'center',
        alignItems: 'center'
    },
    itemNameText: {
        fontFamily: 'Prompt-SemiBold',
        color: '#18467F',
        fontSize: 22
    },
    itemInfoBlock: {
        flex: 0,
        marginTop: 10,
        marginBottom: 15
    },
    itemTopic: {
        flex: 0,
        paddingBottom: 5
    },
    itemTopicText: {
        fontFamily: 'Prompt-SemiBold',
        color: '#3A3A3A',
        fontSize: 16
    },
    itemDescript: {
        paddingLeft: 8
    },
    itemDescriptText: {
        fontFamily: 'Prompt-Regular',
        color: '#3A3A3A',
        fontSize: 15
    }
})

export default ItemDescription;