import React from 'react';
import { View, StyleSheet, Image } from 'react-native';
import { host } from '../../api';

const ItemImage = ({ item }) => {

    return (
        <View style={styles.profileImgSection}>
            <View style={styles.imgBlock}>
                <Image
                    source={
                        item.image === undefined || item.image.filename === ""?
                            { uri: 'https://img.icons8.com/clouds/2x/ingredients.png' }
                            :
                            { uri: host + "/image/" + item.id + "/" + item.image.filename }
                    }
                    style={
                        item.image === undefined || item.image.filename === ""?
                        styles.imageIcon
                        :
                        styles.image
                    }
                />
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    profileImgSection: {
        flex: 0,
        backgroundColor: '#E1E6F2'
    },
    imgBlock: {
        alignItems: 'center',
        justifyContent: 'center',
        height: 230
    },
    image: {
        height: 230,
        width: '100%'
    },
    imageIcon: {
        height: 200,
        width: 200,
    }
})

export default ItemImage;