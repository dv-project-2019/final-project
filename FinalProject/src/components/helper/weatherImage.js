export const checkWeatherImg = (name) => {
    switch (name) {
        case 'Morning':
            return require('../../assets/morning.png');
        case 'Afternoon':
            return require('../../assets/afternoon.png')
        case 'Evening':
            return require('../../assets/evening.png')
        default:
            return require('../../assets/morning.png');
    }
}