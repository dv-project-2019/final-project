import { check, RESULTS, request, openSettings, PERMISSIONS } from 'react-native-permissions';
import { Alert } from 'react-native';

export const requestCameraPermission = ( callback ) => {
    check(PERMISSIONS.ANDROID.CAMERA).then(result => {
        if (result === RESULTS.GRANTED) {
            callback()
        }
        if (result === RESULTS.DENIED) {
            request(PERMISSIONS.ANDROID.CAMERA).then(result => {
                if (result === RESULTS.GRANTED) {
                    callback()
                }
                else if (result === RESULTS.DENIED) {
                    alert('Please, allow to open the camera');
                }
                else if (result === RESULTS.BLOCKED) {
                    alert('Cannot access your camera, please open your setting');
                }
            })
        }
        if (result === RESULTS.BLOCKED) {
            Alert.alert(
                'Alert',
                'Open settings',
                [
                    {
                        text: 'Open settings', 
                        onPress: () => openSettings()
                    },
                    {
                        text: 'Cancel',
                        onPress: () => console.log('Cancel Pressed'),
                        style: 'cancel',
                    }
                ],
                { cancelable: false },
            );

        }
        return
    })
}

export const requestWriteStoragePermission = ( callback ) => {
    check(PERMISSIONS.ANDROID.WRITE_EXTERNAL_STORAGE).then(result => {
        if (result === RESULTS.GRANTED) {
            callback();
        }
        if (result === RESULTS.DENIED) {
            request(PERMISSIONS.ANDROID.WRITE_EXTERNAL_STORAGE).then(result => {
                if (result === RESULTS.GRANTED) {
                    callback();
                }
                else if (result === RESULTS.DENIED) {
                    alert('Please, allow to access your external storage');
                }
                else if (result === RESULTS.BLOCKED) {
                    alert('Cannot access your storage, please open your setting');
                }
            })
            return
        }
        if (result === RESULTS.BLOCKED) {
            Alert.alert(
                'Alert',
                'Open settings',
                [
                    {
                        text: 'Open settings', 
                        onPress: () => openSettings()
                    },
                    {
                        text: 'Cancel',
                        onPress: () => console.log('Cancel Pressed'),
                        style: 'cancel',
                    }
                ]
            );

        }
        return
    })
}

