import React from 'react';
import { StyleSheet, View, SafeAreaView, FlatList, Image, Text } from 'react-native';
import { Card, IconButton, Caption, Chip } from 'react-native-paper';
import NoData from '../noData';
import { dateFormat } from '../helper/dateFormat';
import moment from 'moment';
import { host } from '../../api';

const HistoryList = ({ title, historyDateList, allHistory }) => {

    const checkEmptyData = (data) => {
        if (data.length === 0) {
            return true;
        } else {
            return false;
        }
    }

    const showItemData = (date, allHistory) => {
        let historyList = [];
        allHistory.map((history) => {
            let historyDate = moment(history.CreatedAt).format("D/MM/YYYY")
            if (historyDate === date) {
                historyList.push(history);
            }
        })
        let sortItems = historyList.sort((a, b) => b.item.id - a.item.id)
      
        return sortItems;
    }

    const Item = (historyDate, allHistory) => {
        return (
            <View style={styles.historyList} >
                <View style={styles.histotyDate} >
                    <Caption style={styles.dateText}>{historyDate}</Caption>
                </View>
                {showItemData(historyDate, allHistory).map((history, index) => {
                    return (
                        <Card style={styles.card} key={index}>
                            <View style={styles.cardContent}>
                                <View style={styles.itemImgBlock}>
                                    <Image
                                        source={history.item.image.filename === "" ?
                                            { uri: 'https://img.icons8.com/clouds/2x/ingredients.png' }
                                            :
                                            { uri: host + "/image/" + history.item.id + "/" + history.item.image.filename }
                                        }
                                        style={
                                            history.item.image.filename === "" ?
                                                styles.imageIcon
                                                :
                                                styles.itemImg
                                        }
                                    />
                                </View>
                                <View style={styles.itemData}>
                                    <View style={styles.status}>
                                        {history.item.isExpired ?
                                            <Chip
                                                style={styles.expiredChip}
                                                textStyle={styles.chipText}
                                            >
                                                Expired
                                                </Chip>
                                            :
                                            <Chip
                                                style={styles.ontimeChip}
                                                textStyle={styles.chipText}
                                            >
                                                On time
                                                </Chip>
                                        }
                                    </View>
                                    <View style={styles.itemName}>
                                        <Text style={styles.itemNameText}>{history.item.itemName}</Text>
                                    </View>
                                    <View style={styles.itemName}>
                                        <Text style={styles.itemCalText}>{history.item.calorie} Calories</Text>
                                    </View>
                                    <View style={styles.itemAmount}>
                                        <View style={styles.icon}>
                                            <IconButton
                                                icon='scale'
                                                size={11}
                                                style={styles.iconImg}
                                                color='#414141'
                                            />
                                        </View>
                                        <View style={styles.amount}>
                                            <Text style={styles.amountText}>{history.item.amount}</Text>
                                        </View>
                                        <View style={styles.unit}>
                                            <Text style={styles.amountText}>{history.item.unit}</Text>
                                        </View>
                                    </View>
                                    <View style={styles.category}>
                                        <View style={styles.icon}>
                                            <IconButton
                                                icon='snowflake'
                                                size={11}
                                                style={styles.iconImg}
                                                color='#414141'
                                            />
                                        </View>
                                        <View style={styles.amount}>
                                            <Text style={styles.amountText}>{history.item.category}</Text>
                                        </View>
                                    </View>


                                    <View style={styles.expiration}>
                                        <View style={styles.expirationIcon}>
                                            <IconButton
                                                icon='clock'
                                                size={11}
                                                style={styles.iconImg}
                                                color='#414141'
                                            />
                                        </View>
                                        <View style={styles.expirationDate}>
                                            <Text style={styles.expirationText}>{dateFormat(history.item.expiration_date)}</Text>
                                        </View>
                                    </View>

                                </View>
                            </View>
                        </Card>
                    )
                })}

            </View>
        )
    }

    return (
        <View style={styles.historyListSection}>
            {checkEmptyData(allHistory) ?
                <NoData screen={title} />
                :
                <SafeAreaView style={styles.container}>
                    <FlatList
                        data={historyDateList}
                        renderItem={({ item }) => Item(item, allHistory)}
                        keyExtractor={(item, index) => index.toString()}
                    />
                </SafeAreaView>
            }
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    historyListSection: {
        flex: 1,
        backgroundColor: '#F9F9F9'
    },
    historyList: {
        flex: 0,
        marginTop: 15,
        marginBottom: 5,
        marginLeft: 15,
        marginRight: 15
    },
    histotyDate: {
        flex: 0,
        paddingBottom: 4
    },
    dateText: {
        fontFamily: 'Prompt-Regular',
        fontSize: 14,
    },
    card: {
        flex: 1,
        backgroundColor: 'white',
        marginBottom: 8
    },
    cardContent: {
        flex: 0,
        flexDirection: 'row',
        margin: 5
    },
    itemImgBlock: {
        flex: 0,
        marginLeft: 10,
        marginRight: 10,
        marginBottom: 8,
        marginTop: 5,
        justifyContent: 'center',
        alignItems: 'center'
    },
    itemData: {
        flex: 1,
        marginLeft: 10,
        marginRight: 10,
        marginBottom: 8,
        marginTop: 5
    },
    itemImg: {
        width: 112,
        height: 150
    },
    status: {
        flex: 0,
        marginBottom: 8
    },
    itemName: {
        flex: 0,
        marginBottom: 8,
        paddingLeft: 3
    },
    itemNameText: {
        fontFamily: 'Prompt-SemiBold',
        fontSize: 14,
        color: '#323232'
    },
    itemCalText: {
        fontFamily: 'Prompt-Medium',
        fontSize: 11,
        color: '#414141'
    },
    itemAmount: {
        flex: 0,
        flexDirection: 'row',
        marginBottom: 8
    },
    icon: {
        paddingLeft: 3,
        paddingRight: 5,
        flex: 0,
        justifyContent: 'center'
    },
    amount: {
        flex: 0,
        justifyContent: 'center'
    },
    unit: {
        flex: 0,
        paddingLeft: 4,
        justifyContent: 'center'
    },
    amountText: {
        fontFamily: 'Prompt-Regular',
        fontSize: 11,
        color: '#414141'
    },
    category: {
        flex: 0,
        flexDirection: 'row',
        marginBottom: 8
    },
    iconImg: {
        width: 11,
        height: 11,
        margin: 0
    },
    expiration: {
        flex: 0,
        flexDirection: 'row'
    },
    expirationIcon: {
        paddingRight: 5,
        flex: 0,
        justifyContent: 'center',
        paddingLeft: 3
    },
    expirationDate: {
        flex: 0,
        justifyContent: 'center'
    },
    expirationText: {
        fontFamily: 'Prompt-Regular',
        fontSize: 11,
        color: '#414141'
    },
    expiredText: {
        fontFamily: 'JosefinSans-Medium',
        fontSize: 14,
        color: '#414141'
    },
    expiredChip: {
        backgroundColor: '#F85E5E',
        width: 56,
        height: 20,
        justifyContent: 'center',
        alignItems: 'center'
    },
    ontimeChip: {
        backgroundColor: '#4BD4D4',
        width: 56,
        height: 20,
        justifyContent: 'center',
        alignItems: 'center'
    },
    chipText: {
        fontFamily: 'Prompt-Bold',
        fontSize: 8,
        color: 'white'
    },
    imageIcon: {
        height: 95,
        width: 112,
    }

})

export default HistoryList;