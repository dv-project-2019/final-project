import React from 'react';
import { View, StyleSheet, Text } from 'react-native';
import { IconButton } from 'react-native-paper';
import { useNavigation } from '@react-navigation/native';

const AuthHeader = ({ screen }) => {
    const navigation = useNavigation();

    return (
        <View style={styles.headerRow}>
            <View style={styles.headerStart}>
                <IconButton
                    icon="chevron-left"
                    color='white'
                    size={28}
                    onPress={() => navigation.goBack()}
                />
            </View>
            <View style={styles.headerCenter}>
                {screen === 'Sign Up' ?
                    <Text style={styles.title}>{screen}</Text>
                    :
                    <Text/>
                }
            </View>
            <View style={styles.headerEnd} />
        </View>
    )
}

const styles = StyleSheet.create({
    headerRow: {
        flex: 0,
        flexDirection: 'row',
        height: 50,
        backgroundColor: '#3D55B6'
    },
    headerStart: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'flex-start'
    },
    headerCenter: {
        flex: 2,
        alignItems: 'center',
        justifyContent: 'center',
    },
    headerEnd: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'flex-end'
    },
    title: {
        fontFamily: 'Prompt-Medium',
        fontSize: 16,
        color: 'white'
    }

})

export default AuthHeader;
