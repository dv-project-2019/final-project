import React from 'react';
import { View, StyleSheet, Alert } from 'react-native';
import { Button } from 'react-native-paper';
import { useNavigation } from '@react-navigation/native';
import { connect } from 'react-redux';
import { logout } from '../../api';
import { setToken } from '../../actions/userAction';

const LogoutButton = ({ token, setIsLoading, setToken}) => {
    const navigation = useNavigation();

    const onClickLogout = () => {
        setIsLoading(true);
        logout(token)
            .then(() => {
                setTimeout(() => {
                    setToken("");
                    setIsLoading(false);
                }, 1000);
            })
            .catch((err) => {
                setTimeout(() => {
                    setIsLoading(false);
                    console.log(err);
                    setTimeout(() => {
                        Alert.alert("Error", "Logout is not success, please try again");
                    }, 500);
                }, 1500);
            })
    }

    return (
        <View style={styles.logoutButtonSection}>
            <Button
                mode="contained"
                icon="logout"
                uppercase={false}
                onPress={() => onClickLogout()}
                style={styles.button}
            >
                Logout
                </Button>
        </View>
    )
}

const styles = StyleSheet.create({
    logoutButtonSection: {
        flex: 0,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F9F9F9'
    },
    button: {
        margin: 15,
        padding: 5,
        width: 240,
        backgroundColor: '#F85E5E'
    }
})

const mapStateToProps = (state) => {
    return {
        token: state.user.token
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        setToken: (token) => dispatch(setToken(token))
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(LogoutButton);