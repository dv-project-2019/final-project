import React from 'react';
import { View, StyleSheet, Text } from 'react-native';
import { host } from '../../api';
import { Avatar } from 'react-native-paper';

const ShortBio = ({ user }) => {
    return (
        <View style={styles.shortBioSection}>
            <View style={styles.BioImg}>
                <Avatar.Image
                    size={140}
                    source={
                        user.image.filename === "" ?
                        require('../../assets/userIcon.png')
                        :
                        { uri: host + "/userImage/" + user.id + "/" + user.image.filename }
                    }
                    style={styles.bg}
                />
            </View>
            <View style={styles.name}>
                <Text style={styles.nameText}>{user.firstname} {user.lastname}</Text>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    shortBioSection: {
        flex: 0,
        justifyContent: 'center',
        alignItems: 'center',
        padding: 20,
        backgroundColor: '#4B80D4'
    },
    BioImg: {
        flex: 0,
        margin: 8,
    },
    image: {
        borderRadius: 100,
        width: 130,
        height: 130
    },
    name: {
        flex: 0,
        margin: 8,
        justifyContent: 'center'
    },
    nameText: {
        fontFamily: 'Prompt-SemiBold',
        fontSize: 16,
        color: 'white'
    },
    bg:{
        backgroundColor: '#E3E3E3'
    }
})

export default ShortBio;