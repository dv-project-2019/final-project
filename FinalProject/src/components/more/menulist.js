import React from 'react';
import { View, StyleSheet, Text, TouchableHighlight, Image } from 'react-native';
import { IconButton } from 'react-native-paper';
import { useNavigation } from '@react-navigation/native';

const MenuList = () => {
    const navigation = useNavigation();

    return (
        <View style={styles.menuSection}>
            <TouchableHighlight
                activeOpacity={0.8}
                underlayColor="#206DCA"
                style={styles.overviewHightlight}
                onPress={() => navigation.navigate('Profile')}
            >
                <View style={styles.optionBlock}>
                    <View style={styles.icon} >
                        <Image
                            source={require('../../assets/user.png')}
                            style={styles.iconImage}
                        />
                    </View>
                    <View style={styles.optionName}>
                        <Text style={styles.optionNameText}>Profile</Text >
                    </View>
                    <View style={styles.optionIcon}>
                        <IconButton
                            icon='chevron-right'
                            size={20}
                            color='#18467F'
                        />
                    </View>
                </View>
            </TouchableHighlight>
        </View>
    )
}

const styles = StyleSheet.create({
    menuSection: {
        flex: 1,
        backgroundColor: '#F9F9F9',
        padding: 10
    },
    icon: {
        flex: 0,
        justifyContent: 'center'
    },
    iconImage: {
        width: 24,
        height: 24
    },
    optionBlock: {
        flex: 0,
        flexDirection: 'row',
        paddingTop: 10,
        paddingLeft: 20,
        paddingBottom: 10,
        paddingRight: 10,
        borderBottomWidth: 1,
        borderBottomColor: '#CCCCCC',
        backgroundColor: '#FFFFFF',
        borderTopLeftRadius: 5,
        borderTopRightRadius: 5
    },
    optionName: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'flex-start',
        paddingLeft: 15
    },
    optionIcon: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'flex-end'
    },
    optionNameText: {
        fontFamily: 'Prompt-Medium',
        fontSize: 14,
        color: '#323232'
    },
    overviewHightlight: {
        borderTopLeftRadius: 5,
        borderTopRightRadius: 5
    }
})

export default MenuList;