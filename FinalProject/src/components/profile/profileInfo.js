import React from 'react';
import { View, StyleSheet, Text } from 'react-native';
import { Divider } from 'react-native-paper';
import { dateFormat } from '../helper/dateFormat';
 
const ProfileInfo = ({ userInformation }) => {
    return (
        <View style={styles.profileInfoSection}>
            <View style={styles.inputBlock}>
                <View style={styles.topic}>
                    <Text style={styles.topicText}>Email</Text>
                </View>
                <View style={styles.userInfo}>
                    <Text style={styles.userInfoText}>{userInformation.email}</Text>
                </View>
            </View>
            <Divider />
            <View style={styles.inputBlock}>
                <View style={styles.topic}>
                    <Text style={styles.topicText}>First Name</Text>
                </View>
                <View style={styles.userInfo}>
                    <Text style={styles.userInfoText}>{userInformation.firstname}</Text>
                </View>
            </View>
            <Divider />
            <View style={styles.inputBlock}>
                <View style={styles.topic}>
                    <Text style={styles.topicText}>Last Name</Text>
                </View>
                <View style={styles.userInfo}>
                    <Text style={styles.userInfoText}>{userInformation.lastname}</Text>
                </View>
            </View>
            <Divider />
            <View style={styles.inputBlock}>
                <View style={styles.topic}>
                    <Text style={styles.topicText}>Date Of Birth</Text>
                </View>
                <View style={styles.userInfo}>
                    <Text style={styles.userInfoText}>{dateFormat(userInformation.dob)}</Text>
                </View>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    profileInfoSection: {
        flex: 1,
        backgroundColor: '#F9F9F9',
        padding: 10
    },
    inputBlock: {
        flex: 1,
        marginLeft: 15,
        marginRight: 15,
        marginBottom: 10,
        marginTop: 10,
        justifyContent: 'center'
    },
    topic: {
        flex: 0,
        paddingBottom: 8
    },
    topicText:{
        fontFamily: 'Prompt-Regular',
        color: '#414141',
        fontSize: 13
    },
    userInfo: {
        flex: 0,
    },
    userInfoText: {
        fontFamily: 'Prompt-Regular',
        color: '#18467F',
        fontSize: 16
    }
})

export default ProfileInfo;