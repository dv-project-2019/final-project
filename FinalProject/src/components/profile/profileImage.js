import React from 'react';
import { View, StyleSheet } from 'react-native';
import { Avatar } from 'react-native-paper';
import { host } from '../../api';

const ProfileImage = ({ profile }) => {
    return (
        <View style={styles.profileImgSection}>
            <View style={styles.imgBlock}>
                <Avatar.Image
                    size={140}
                    source={
                        profile.image.filename === "" ?
                        require('../../assets/userIcon.png')
                        :
                        { uri: host + "/userImage/" + profile.id + "/" + profile.image.filename }
                    }
                    style={styles.bg}
                />
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    profileImgSection: {
        flex: 0,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#4B80D4'
    },
    imgBlock: {
        flex: 0,
        margin: 35
    },
    bg:{
        backgroundColor: '#E3E3E3'
    }
})

export default ProfileImage;