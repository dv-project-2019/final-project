import React from 'react';
import { View, Text, StyleSheet, Image } from 'react-native';

const LogoImage = () => {
    return (
        <View style={styles.logo}>
            <View style={styles.image}>
                <Image
                    source={require('../../assets/logo.png')}
                    style={styles.logoImg}
                />
            </View>
            <View style={styles.logoName}>
                <Text style={styles.logoNameText}>Eat On Time</Text>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    logo: {
        flex: 0,
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 100,
        paddingBottom: 50
    },
    image: {
        flex: 0,
        marginTop: 20
    },
    logoImg: {
        width: 200,
        height: 200
    },
    logoName: {
        flex: 0,
        margin: 10
    },
    logoNameText: {
        color: 'white',
        fontSize: 28,
        fontFamily: 'Prompt-SemiBold'
    }
})

export default LogoImage;