import React from 'react';
import { View, StyleSheet } from 'react-native';
import { Button } from 'react-native-paper';
import { useNavigation } from '@react-navigation/native';

const OptionButtons = () => {
    const navigation = useNavigation();

    return (
        <View style={styles.optionButtons}>
            <View style={styles.button}>
                <Button
                    mode="contained"
                    color='#7795FF'
                    uppercase={false}
                    labelStyle={styles.loginButtonText}
                    style={styles.loginButton}
                    onPress={() => navigation.navigate('Login')}>
                    Login
                    </Button>
            </View>
            <View style={styles.button}>
                <Button
                    mode="contained"
                    color='#FFFFFF'
                    uppercase={false}
                    labelStyle={styles.registerButtonText}
                    style={styles.registerButton}
                    onPress={() => navigation.navigate('Register')}>
                    Sign Up
                    </Button>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    optionButtons: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'center'
    },
    button: {
        flex: 0,
        margin: 10
    },
    loginButton: {
        borderRadius: 30,
        width: 280,
        height: 48,
        justifyContent: 'center'
    },
    loginButtonText: {
        color: 'white',
        fontSize: 16,
        fontFamily: 'Prompt-Medium'
    },
    registerButton: {
        borderRadius: 30,
        width: 280,
        height: 48,
        justifyContent: 'center'
    },
    registerButtonText: {
        color: '#5E5E5E',
        fontSize: 16,
        fontFamily: 'Prompt-Medium'
    }
})

export default OptionButtons;