import React from 'react';
import { View, StyleSheet, Image } from 'react-native';

const TopicImage = () => {
    return (
        <View style={styles.topicImageSection}>
            <View style={styles.imageBlock}>
                <Image
                    source={require('../../assets/basket.png')}
                    style={styles.image}
                />
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    topicImageSection: {
        flex: 0,
        backgroundColor: '#E1E6F2',
        justifyContent: 'center',
        alignItems: 'center'
    },
    imageBlock: {
        flex: 0,
        margin: 20,
        padding: 25
    },
    image: {
        width: 125,
        height: 125
    }
})
export default TopicImage;