import React, { useState } from 'react';
import { View, StyleSheet, Text, SafeAreaView, FlatList, TouchableHighlight, Image } from 'react-native';
import { Card, IconButton, Button, ActivityIndicator } from 'react-native-paper';
import moment from 'moment';
import NoData from '../noData';
import { useNavigation } from '@react-navigation/native';
import { host, removeItem } from '../../api';

const ItemList = ({ title, itemsData, categoryValue, searchValue, token, refreshData}) => {
    const navigation = useNavigation();
    const [isLoading, setIsLoading] = useState(false);
    const [removeItemId, setRemoveItemId] = useState(0);

    const dateFormat = (date) => {
        return moment(date).format("DD/MM/YYYY");
    }

    const checkEmptyData = (data) => {
        if (data.length === 0) {
            return true;
        } else {
            return false;
        }
    }

    const checkFilter = (categoryValue, items, searchValue) => {
        if (categoryValue === 'All' && searchValue === '') {
            return items;
        } else if(categoryValue === 'All') {
            return items.filter((item) => item.itemName.toLowerCase().match(searchValue.toLowerCase()));
        } else{
            return items.filter((item) => item.category.toLowerCase().match(categoryValue.toLowerCase()) && item.itemName.toLowerCase().match(searchValue.toLowerCase()));
        }
    }

    const clickOnRemove = (item) => {
        setIsLoading(true);
        setRemoveItemId(item.id);
        removeItem(item.id, token)
        .then(() => {
            setTimeout(() => {
                setIsLoading(false);
                refreshData();
            }, 500);
        })
        .catch((err) => {
            console.log(err);
            setTimeout(() => {
                setIsLoading(false);
            }, 500);
        })
    }

    const Item = (item) => {
        return (
            <View style={styles.mainBlock} >
                <TouchableHighlight
                    activeOpacity={0.8}
                    underlayColor="#206DCA"
                    style={styles.touchHighlight}
                    onPress={() => navigation.navigate('ItemInfo', { itemId: item.id})}
                >
                    <Card style={styles.card}>
                        <View style={styles.cardContent}>
                            <View style={styles.itemImgBlock}>
                                <Image
                                    source={
                                        item.image.filename === ""?
                                        { uri : 'https://img.icons8.com/clouds/2x/ingredients.png'}
                                        :
                                        { uri: host + "/image/" + item.id + "/" + item.image.filename }
                                    }
                                    style={styles.itemImg}
                                />
                            </View>
                            <View style={styles.itemData}>
                                <View style={styles.itemName}>
                                    <Text style={styles.itemNameText}>{item.itemName}</Text>
                                </View>
                                <View style={styles.itemName}>
                                    <Text style={styles.itemCalText}>{item.calorie} Calories</Text>
                                </View>
                                <View style={styles.itemAmount}>
                                    <View style={styles.icon}>
                                        <IconButton
                                            icon='scale'
                                            size={11}
                                            style={styles.iconImg}
                                            color='#414141'
                                        />
                                    </View>
                                    <View style={styles.amount}>
                                        <Text style={styles.amountText}>{item.amount}</Text>
                                    </View>
                                    <View style={styles.unit}>
                                        <Text style={styles.amountText}>{item.unit}</Text>
                                    </View>
                                </View>
                                <View style={styles.category}>
                                    <View style={styles.icon}>
                                        <IconButton
                                            icon='snowflake'
                                            size={11}
                                            style={styles.iconImg}
                                            color='#414141'
                                        />
                                    </View>
                                    <View style={styles.amount}>
                                        <Text style={styles.amountText}>{item.category}</Text>
                                    </View>
                                </View>

                                {item.isExpired?
                                    <View style={styles.expiration}>
                                        <View style={styles.expirationIcon}>
                                            <IconButton
                                                icon='clock'
                                                size={11}
                                                style={styles.iconImg}
                                                color='#C62020'
                                            />
                                        </View>
                                        <View style={styles.expirationDate}>
                                            <Text style={styles.expiredText}>{dateFormat(item.expiration_date)}</Text>
                                        </View>
                                    </View>
                                    :
                                    <View style={styles.expiration}>
                                        <View style={styles.expirationIcon}>
                                            <IconButton
                                                icon='clock'
                                                size={11}
                                                style={styles.iconImg}
                                                color='#239B56'
                                            />
                                        </View>
                                        <View style={styles.expirationDate}>
                                            <Text style={styles.expirationText}>{dateFormat(item.expiration_date)}</Text>
                                        </View>
                                    </View>
                                }
                            </View>
                        </View>
                        <Card.Actions style={styles.cardFooter}>
                            <View style={styles.buttonBlock}>
                                {isLoading && item.id === removeItemId ?
                                <View style={{height: 35, justifyContent: 'center', marginRight:16}}>

                                <ActivityIndicator size="small" color="#3FC6DD"></ActivityIndicator>
                                </View>
                                :
                                item.isExpired ?
                                    <Button
                                        mode="contained"
                                        uppercase={false}
                                        onPress={() => clickOnRemove(item)}
                                        style={styles.expiredButton}
                                    >
                                        Expired
                                    </Button>
                                    :
                                    <Button
                                        mode="contained"
                                        uppercase={false}
                                        onPress={() => clickOnRemove(item)}
                                        style={styles.confirmButton}
                                    >
                                        Eat
                                       
                                </Button>
                            
                                
                                }

                            </View>

                        </Card.Actions>
                    </Card>
                </TouchableHighlight>
            </View>
        )
    }

    return (
        <View style={styles.listSection}>
            {checkEmptyData(itemsData) ?
                <NoData screen={title} />
                :
                <SafeAreaView style={styles.container}>
                    <FlatList
                        data={checkFilter(categoryValue, itemsData, searchValue)}
                        renderItem={({ item }) => Item(item)}
                        keyExtractor={(item, index) => index.toString()}
                        ListEmptyComponent={() => <NoData screen={categoryValue} />}
                    />
                </SafeAreaView>
            }

        </View>
    )
}

const styles = StyleSheet.create({
    listSection: {
        flex: 1,
        backgroundColor: '#F9F9F9',
        padding: 5
    },
    container: {
        flex: 1
    },
    mainBlock: {
        flex: 1,
        marginLeft: 10,
        marginRight: 10,
        marginTop: 5,
        marginBottom: 5
    },
    touchHighlight: {
        borderRadius: 5
    },
    card: {
        flex: 1,
        backgroundColor: 'white'
    },
    cardContent: {
        flex: 0,
        flexDirection: 'row'
    },
    itemImgBlock: {
        flex: 0,
        margin: 10
    },
    itemData: {
        flex: 1,
        marginLeft: 10,
        marginRight: 10,
        marginBottom: 8,
        marginTop: 5
    },
    itemImg: {
        width: 112,
        height: 112
    },
    nameRow: {
        flex: 1,
        flexDirection: 'row'
    },
    itemName: {
        flex: 0,
        marginBottom: 8
    },
    editIconBlock: {
        flex: 0,
        justifyContent: 'flex-start',
        alignItems: 'flex-end',
        position: 'absolute',
        right: 0,
        top: 0
    },
    editIcon: {
        width: 16,
        height: 16,
        marginTop: 5,
        marginLeft: 5,
        marginRight: 8
    },
    itemNameText: {
        fontFamily: 'Prompt-SemiBold',
        fontSize: 14,
        color: '#323232'
    },
    itemCalText: {
        fontFamily: 'Prompt-Medium',
        fontSize: 11,
        color: '#414141'
    },
    itemAmount: {
        flex: 0,
        flexDirection: 'row',
        marginBottom: 8
    },
    icon: {
        paddingRight: 5,
        flex: 0,
        justifyContent: 'center'
    },
    amount: {
        flex: 0,
        justifyContent: 'center'
    },
    unit: {
        flex: 0,
        paddingLeft: 4,
        justifyContent: 'center'
    },
    amountText: {
        fontFamily: 'Prompt-Regular',
        fontSize: 11,
        color: '#414141'
    },
    category: {
        flex: 0,
        flexDirection: 'row',
        marginBottom: 8
    },
    iconImg: {
        width: 11,
        height: 11,
        margin: 0
    },
    expiration: {
        flex: 0,
        flexDirection: 'row'
    },
    expirationIcon: {
        paddingRight: 5,
        flex: 0,
        justifyContent: 'center'
    },
    expirationDate: {
        flex: 0,
        justifyContent: 'center'
    },
    expirationText: {
        fontFamily: 'Prompt-Regular',
        fontSize: 11,
        color: '#414141'
    },
    expiredText: {
        fontFamily: 'Prompt-Regular',
        fontSize: 11,
        color: '#C62020'
    },
    cardFooter: {
        backgroundColor: '#EFEFEF',
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'center',
        borderBottomLeftRadius: 4,
        borderBottomRightRadius: 4
    },
    buttonBlock: {
        flex: 0,
        margin: 5,
        alignItems: 'flex-end'
    },
    expiredButton: {
        backgroundColor: '#F85E5E',
        width: 136,
        height: 35,
        justifyContent: 'center'
    },
    confirmButton: {
        backgroundColor: '#4B80D4',
        width: 136,
        height: 35,
        justifyContent: 'center'
    }
})


export default ItemList;