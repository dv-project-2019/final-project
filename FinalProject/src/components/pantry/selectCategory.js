import React from 'react';
import { View, StyleSheet, Image, Text, TouchableOpacity } from 'react-native';

const SelectCategory = ({ selectedValue, setSelectedValue }) => {

    const onSelectCategory = (value) => {
        setSelectedValue(value);
    }

    const checkCategorySelected = (category) => {
        if (category === selectedValue) {
            return styles.selectedBox;
        } else {
            return styles.selectBox;
        }
    }

    const checkCategoryTextFocus = (category) => {
        if (category === selectedValue) {
            return styles.nametextFocus;
        } else {
            return styles.nameText;
        }
    }

    return (
        <View style={styles.categorySection}>
            <View style={styles.selectBlock}>
                <TouchableOpacity
                    onPress={() => onSelectCategory('All')}
                >
                    <View style={styles.filterButton}>
                        <View style={checkCategorySelected('All')}>
                            <Image
                                style={styles.iconImg}
                                source={require('../../assets/kitchen.png')
                                }
                            />
                        </View>
                        <View style={styles.iconName}>
                            <Text style={checkCategoryTextFocus('All')}>All</Text>
                        </View>
                    </View>
                </TouchableOpacity>

                <TouchableOpacity
                    onPress={() => onSelectCategory('Fridge')}
                >
                    <View style={styles.filterButton}>
                        <View style={checkCategorySelected('Fridge')}>
                            <Image
                                style={styles.iconImg}
                                source={require('../../assets/fridge.png')
                                }
                            />
                        </View>
                        <View style={styles.iconName}>
                            <Text style={checkCategoryTextFocus('Fridge')}>Fridge</Text>
                        </View>
                    </View>
                </TouchableOpacity>

                <TouchableOpacity
                    onPress={() => onSelectCategory('Freezer')}
                >
                    <View style={styles.filterButton}>
                        <View style={checkCategorySelected('Freezer')}>
                            <Image
                                style={styles.iconImg}
                                source={require('../../assets/freezer.png')
                                }
                            />
                        </View>
                        <View style={styles.iconName}>
                            <Text style={checkCategoryTextFocus('Freezer')}>Freezer</Text>
                        </View>
                    </View>
                </TouchableOpacity>


                <TouchableOpacity
                    onPress={() => onSelectCategory('Couter')}
                >
                    <View style={styles.filterButton}>
                        <View style={checkCategorySelected('Couter')}>
                            <Image
                                style={styles.iconImg}
                                source={require('../../assets/shelf.png')
                                }
                            />
                        </View>
                        <View style={styles.iconName}>
                            <Text style={checkCategoryTextFocus('Couter')}>Couter</Text>
                        </View>
                    </View>
                </TouchableOpacity>

            </View>
        </View>

    )
}

const styles = StyleSheet.create({
    categorySection: {
        flex: 0,
        paddingBottom: 5,
        backgroundColor: '#FFFFFF'
    },
    selectBlock: {
        flex: 0,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: 15,
        marginRight: 15,
        paddingBottom: 8
    },
    filterButton: {
        flex: 0,
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: 15,
        marginRight: 15
    },
    selectBox: {
        flex: 0,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F1F1F1',
        margin: 5,
        width: 45,
        height: 45,
        borderRadius: 100
    },
    selectedBox: {
        flex: 0,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#C5D8F6',
        margin: 5,
        width: 50,
        height: 50,
        borderRadius: 100
    },
    iconImg: {
        width: 24,
        height: 24
    },
    iconName: {
        flex: 0
    },
    nameText: {
        fontFamily: 'Prompt-Light',
        fontSize: 12,
        color: '#2B2B2B'
    },
    nametextFocus: {
        fontFamily: 'Prompt-SemiBold',
        fontSize: 12,
        color: '#323232'
    },
})

export default SelectCategory;