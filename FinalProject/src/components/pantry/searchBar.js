import React from 'react';
import { View, StyleSheet } from 'react-native';
import { Searchbar } from 'react-native-paper';

const SearchBar = ({ searchValue, setSearchValue }) => {
    const onChangeSearch = (text) => {
        setSearchValue(text);
    }

    return (
        <View style={styles.searchSection} >
            <Searchbar
                placeholder="Search item"
                onChangeText={text => onChangeSearch(text)}
                value={searchValue}
                style={styles.searchBar}
                inputStyle={styles.input}
            />
        </View>
    )
}

const styles = StyleSheet.create({
    searchSection: {
        flex: 0,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#FFFFFF'
    },
    searchBar: {
        margin: 10,
        height: 40,
        width: '90%',
        justifyContent: 'center'
    },
    input: {
        fontSize: 12,
        fontFamily: 'Prompt-Medium'
    }
})

export default SearchBar;