import React from 'react';
import { View, StyleSheet, TouchableHighlight, Alert } from 'react-native';
import { Avatar } from 'react-native-paper';
import { host, uploadUserImage } from '../../api';
import { requestCameraPermission, requestWriteStoragePermission } from '../helper/uploadImagePermission';
import ImagePicker from 'react-native-image-picker';

const ProfileImage = ({ userImage, setUserImage, userInfo, token }) => {

    const upload = () => {
        ImagePicker.showImagePicker({}, (image) => {
            if (image.didCancel) {
                console.log('User cancelled image picker');
            } else if (image.error) {
                console.log('ImagePicker Error: ', image.error);
            } else {
                setUserImage(image);
                uploadUserImage(image, token)
                    .then((res) => {
                        setTimeout(() => {
                            Alert.alert("Success", 'Profile image uploaded');
                        }, 1000);
                    })
                    .catch((err) => {
                        setTimeout(() => {
                            Alert.alert("Error", 'Upload profile image is not success, please try again');
                        }, 1000);
                    })
            }
        });
    }

    const uploadProfileImage = () => {
        requestCameraPermission(() => requestWriteStoragePermission(upload));
    }

    return (
        <View style={styles.profileImgSection}>
            <View style={styles.imgBlock}>
                <Avatar.Image
                    size={140}
                    source={
                        userInfo.image.filename === "" && userImage === "" ?
                            require('../../assets/userIcon.png')
                            :
                            userImage === "" ?
                                { uri: host + "/userImage/" + userInfo.id + "/" + userInfo.image.filename }
                                :
                                { uri: userImage.uri }
                    }
                    style={styles.bg}
                />
            </View>
            <View style={styles.cameraIconBlock}>
                <TouchableHighlight
                    activeOpacity={0.8}
                    underlayColor="#4B80D4"
                    onPress={() => uploadProfileImage()}
                    style={styles.touchHighlight}
                >
                    <Avatar.Icon
                        size={38}
                        icon="camera"
                        color="#18467F"
                        style={styles.cameraIcon}
                    />
                </TouchableHighlight>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    profileImgSection: {
        flex: 0,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#4B80D4'
    },
    imgBlock: {
        flex: 0,
        margin: 35
    },
    cameraIconBlock: {
        position: 'absolute',
        right: 110,
        top: 40
    },
    cameraIcon: {
        backgroundColor: '#F1F1F1'
    },
    touchHighlight: {
        borderRadius: 100
    },
    bg:{
        backgroundColor: '#E3E3E3'
    }
})

export default ProfileImage;