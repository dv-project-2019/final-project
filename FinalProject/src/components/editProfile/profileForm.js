import React, { useState, useRef } from 'react';
import { View, StyleSheet, Text } from 'react-native';
import { TextInput, IconButton, Button, ActivityIndicator } from 'react-native-paper';
import DateTimePickerModal from "react-native-modal-datetime-picker";
import { dateFormat } from '../helper/dateFormat';
import * as yup from 'yup';
import { Formik } from 'formik';

const ProfileForm = ({ profileInfo, setProfileInfo, updateProfile}) => {
    const [visible, setVisible] = useState(false);
    const firstnameInput = useRef();
    const lastnameInput = useRef();
    const dobInput = useRef();

    const showDatePicker = () => {
        setVisible(true);
    }

    const hideDatePicker = () => {
        setVisible(false);
    }

    const handleConfirm = (date) => {
        hideDatePicker();
        setProfileInfo({
            ...profileInfo,
            dob: date.getTime()
        });
    }

    const userInfoSchema = yup.object().shape({
        firstname: yup.string()
            .required('First name is required'),
        lastname: yup.string()
            .required('Last name is required'),
    });

    return (
        <Formik
            validationSchema={userInfoSchema}
            initialValues={{
                firstname: profileInfo.firstname,
                lastname: profileInfo.lastname,
            }}
            enableReinitialize={false}
            onSubmit={(values, actions) => {
                updateProfile(values);
                setTimeout(() => {
                    actions.setSubmitting(false);
                }, 1000);
            }}
        >
            {({ handleChange, handleSubmit, handleBlur, values, errors, touched, isSubmitting }) => (
                <View style={styles.profileInfoSection}>
                    <View style={styles.form}>
                        <View style={styles.textInputBlock}>
                            <View style={styles.topic}>
                                <Text style={styles.topicText}>First Name</Text>
                            </View>
                            <View style={styles.textInput}>
                                <TextInput
                                    name="firstname"
                                    mode='outlined'
                                    value={values.firstname}
                                    style={styles.input}
                                    onChangeText={handleChange('firstname')}
                                    onBlur={handleBlur('firstname')}
                                    ref={firstnameInput}
                                    autoCapitalize="none"
                                    returnKeyType="next"
                                    onSubmitEditing={() => lastnameInput.current.focus()}
                                />
                            </View>
                            <View style={styles.validateError}>
                                {errors.firstname && touched.firstname ? (
                                    <Text style={styles.errorText}>{errors.firstname}</Text>
                                ) : null}
                            </View>
                        </View>
                        <View style={styles.textInputBlock}>
                            <View style={styles.topic}>
                                <Text style={styles.topicText}>Last Name</Text>
                            </View>
                            <View style={styles.textInput}>
                                <TextInput
                                    name="lastname"
                                    mode='outlined'
                                    value={profileInfo.lastname}
                                    value={values.lastname}
                                    style={styles.input}
                                    onChangeText={handleChange('lastname')}
                                    onBlur={handleBlur('lastname')}
                                    ref={lastnameInput}
                                    autoCapitalize="none"
                                    returnKeyType="next"
                                    onSubmitEditing={() => dobInput.current.focus()}
                                />
                            </View>
                            <View style={styles.validateError}>
                                {errors.lastname && touched.lastname ? (
                                    <Text style={styles.errorText}>{errors.lastname}</Text>
                                ) : null}
                            </View>
                        </View>
                        <View style={styles.textInputBlock}>
                            <View style={styles.topic}>
                                <Text style={styles.topicText}>Date Of Birth</Text>
                            </View>
                            <View style={styles.rowBlock}>
                                <View style={styles.dateInput}>
                                    <TextInput
                                        mode='outlined'
                                        placeholder='Date of birth'
                                        value={dateFormat(profileInfo.dob)}
                                        style={styles.input}
                                        ref={dobInput}
                                        autoCapitalize="none"
                                        returnKeyType="next"
                                    />
                                </View>
                                <View style={styles.dateIcon}>
                                    <DateTimePickerModal
                                        isVisible={visible}
                                        mode="date"
                                        onConfirm={handleConfirm}
                                        onCancel={hideDatePicker}
                                    />
                                    <IconButton
                                        icon='calendar-month'
                                        size={23}
                                        color='#18467F'
                                        onPress={() => showDatePicker()}
                                    />
                                </View>
                            </View>
                        </View>

                        <View style={styles.buttonBlock}>
                            {isSubmitting ? (
                                <ActivityIndicator size="small" color="#3FC6DD"></ActivityIndicator>
                            ) : (
                                    <Button
                                        mode="contained"
                                        uppercase={false}
                                        onPress={handleSubmit}
                                        style={styles.addButton}
                                    >
                                        Update
                                    </Button>
                                )}

                        </View>
                    </View>
                </View>
            )
            }
        </Formik >
    )
}

const styles = StyleSheet.create({
    profileInfoSection: {
        flex: 1,
        backgroundColor: '#F9F9F9'
    },
    form: {
        flex: 0,
        margin: 15
    },
    textInputBlock: {
        flex: 1,
        marginLeft: 20,
        marginRight: 20,
        marginTop: 5,
        marginBottom: 15
    },
    topic: {
        marginTop: 5
    },
    topicText: {
        fontFamily: 'Prompt-SemiBold',
        fontSize: 16,
        color: '#323232'
    },
    input: {
        height: 48,
        backgroundColor: '#F9F9F9'
    },
    rowBlock: {
        flex: 1,
        flexDirection: 'row'
    },
    dateInput: {
        flex: 1
    },
    dateIcon: {
        flex: 0,
        justifyContent: 'center'
    },
    buttonBlock: {
        flex: 0,
        marginTop: 15,
        marginBottom: 15,
        padding: 10,
        justifyContent: 'center',
        alignItems: 'center'
    },
    addButton: {
        width: '40%',
        backgroundColor: '#4B80D4'
    },
    validateError: {
        flex: 0,
        marginLeft: 5,
        marginRight: 5,
        marginTop: 2,
        marginBottom: 2
    },
    errorText: {
        fontFamily: 'Prompt-Medium',
        flex: 0,
        color: 'red',
        fontSize: 12
    }
})

export default ProfileForm;