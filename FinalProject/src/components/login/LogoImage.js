import React from 'react';
import { View, Text, StyleSheet, Image } from 'react-native';

const LogoImage = () => {
    return (
        <View style={styles.logo}>
            <View style={styles.image}>
                <Image
                    source={require('../../assets/logo.png')}
                    style={styles.logoImg}
                />
            </View>
            <View style={styles.logoName}>
                <Text style={styles.logoNameText}>Eat On Time</Text>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    logo: {
        flex: 0,
        justifyContent: 'center',
        alignItems: 'center',
        padding: 10
    },
    image: {
        flex: 0,
        marginTop: 10
    },
    logoImg: {
        width: 140,
        height: 140
    },
    logoName: {
        flex: 0,
        margin: 10
    },
    logoNameText: {
        color: 'white',
        fontSize: 24,
        fontFamily: 'Prompt-SemiBold'
    }
})

export default LogoImage;