import React, { useState, useRef } from 'react';
import { View, StyleSheet, Text, TouchableOpacity, Alert } from 'react-native';
import { Card, TextInput, Button, ActivityIndicator } from 'react-native-paper';
import { useNavigation } from '@react-navigation/native';
import { login } from '../../api';
import { setToken } from '../../actions/userAction';
import { connect } from 'react-redux';

const LoginForm = ({ setToken }) => {
    const navigation = useNavigation();
    const emailInput = useRef();
    const passwordInput = useRef();
    const [loginForm, setLoginForm] = useState({
        email: "",
        password: ""
    });
    const [isLoading, setIsLoading] = useState(false);

    const onClickLogin = (form) => {
        setIsLoading(true);
        login(form)
            .then((res) => {   
                setTimeout(() => {
                    setIsLoading(false);
                    setToken(res.data.token);
                    setLoginForm({
                        email: "",
                        password: ""
                    });
                }, 1000);
            })
            .catch((err) => {
                console.log(JSON.stringify(err));
                setTimeout(() => {
                    setIsLoading(false);
                    Alert.alert("Error", 'Email or Password is not correct');
                }, 1000);

            })
    }

    return (
        <View style={styles.form}>
            <Card style={styles.card}>
                <View style={styles.topicBlock}>
                    <View style={styles.topic}>
                        <Text style={styles.welcomeText}>Welcome</Text>
                    </View>
                    <View style={styles.topic}>
                        <Text style={styles.subTitleText}>Login into your account</Text>
                    </View>
                </View>
                <View style={styles.textInputSection}>
                    <View style={styles.textInputBlock}>
                        <View style={styles.title}>
                            <Text style={styles.titleText}> Email </Text>
                        </View>
                        <View style={styles.textInput}>
                            <TextInput
                                ref={emailInput}
                                mode='outlined'
                                placeholder='email'
                                value={loginForm.email}
                                style={styles.input}
                                onChangeText={(text) => setLoginForm({
                                    ...loginForm,
                                    email: text
                                })}
                                autoCapitalize="none"
                                returnKeyType="next"
                                onSubmitEditing={() => passwordInput.current.focus()}
                            />
                        </View>
                    </View>
                    <View style={styles.textInputBlock}>
                        <View style={styles.title}>
                            <Text style={styles.titleText}> Password </Text>
                        </View>
                        <View style={styles.textInput}>
                            <TextInput
                                ref={passwordInput}
                                mode='outlined'
                                placeholder='password'
                                value={loginForm.password}
                                style={styles.input}
                                onChangeText={(text) => setLoginForm({
                                    ...loginForm,
                                    password: text
                                })}
                                autoCapitalize="none"
                                returnKeyType="next"
                                secureTextEntry={true}
                            />
                        </View>
                    </View>
                    <View style={styles.button}>
                        {isLoading ?
                            <ActivityIndicator size="small" color="#3FC6DD"></ActivityIndicator>
                            :
                            <Button
                                mode="contained"
                                color='#4B80D4'
                                uppercase={false}
                                labelStyle={styles.loginButtonText}
                                style={styles.loginButton}
                                onPress={() => onClickLogin(loginForm)}
                            >
                                Login
                        </Button>
                        }

                    </View>
                </View>
                <View style={styles.regisBlock} >
                    <View style={styles.normalTextBlock}>
                        <Text style={styles.normalText}>Don't have an account?</Text>
                    </View>
                    <View style={styles.linkTextBlock}>
                        <TouchableOpacity onPress={() => navigation.navigate('Register')}>
                            <Text style={styles.regisLinkText}>Register Now</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </Card>

        </View>
    )
}

const styles = StyleSheet.create({
    form: {
        flex: 1
    },
    card: {
        marginLeft: 15,
        marginRight: 15,
        marginTop: 3,
        marginBottom: 5,
        padding: 10,
        backgroundColor: '#F9F9F9'
    },
    topicBlock: {
        flex: 0,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 5,
        marginBottom: 5
    },
    topic: {
        padding: 5
    },
    welcomeText: {
        color: '#323232',
        fontSize: 18,
        fontFamily: 'Prompt-SemiBold'
    },
    subTitleText: {
        color: '#323232',
        fontSize: 16,
        fontFamily: 'Prompt-Medium'
    },
    textInputSection: {
        flex: 0,
        marginTop: 0,
        marginBottom: 0,
        marginLeft: 30,
        marginRight: 30
    },
    textInputBlock: {
        flex: 0,
        paddingTop: 10,
        paddingBottom: 10
    },
    title: {
        paddingBottom: 3
    },
    titleText: {
        color: '#323232',
        fontSize: 14,
        fontFamily: 'Prompt-SemiBold'
    },
    textInput: {
        flex: 0
    },
    input: {
        height: 48,
        backgroundColor: 'white'
    },
    button: {
        flex: 0,
        margin: 5,
        paddingBottom: 10,
        paddingTop: 20,
        alignItems: 'center'
    },
    loginButton: {
        width: 160,
        height: 48,
        justifyContent: 'center'
    },
    loginButtonText: {
        color: 'white',
        fontSize: 16,
        fontFamily: 'Prompt-Medium'
    },
    regisBlock: {
        flex: 0,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        paddingTop: 8
    },
    normalTextBlock: {
        flex: 0,
        paddingRight: 5
    },
    normalText: {
        color: '#707070',
        fontSize: 13,
        fontFamily: 'Prompt-Regular'
    },
    linkTextBlock: {
        flex: 0,
        paddingRight: 5
    },
    regisLinkText: {
        fontSize: 14,
        fontFamily: 'Prompt-SemiBold',
        color: '#4481BD',
        textDecorationLine: 'underline'
    },
})

const mapDispatchToProps = (dispatch) => {
    return {
        setToken: (token) => dispatch(setToken(token))
    }
}
export default connect(null, mapDispatchToProps)(LoginForm);