import React  from 'react';
import { StyleSheet, View, Image } from 'react-native';
import { Caption } from 'react-native-paper';

const NoDataOnSelected = () => {
    return (
        <View style={styles.noDataSection}>
            <View style={styles.imgSection}>
                <Image
                    source={require('../../assets/empty-fridge.png')}
                    style={styles.img}
                />
            </View>
            <View style={styles.textSection}>
                <Caption style={styles.text}>No Data</Caption>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    noDataSection: {
        flex: 1,
        marginTop: 124,
        justifyContent: 'center',
        alignItems: 'center'
    },
    imgSection: {
        flex: 1
    },
    img: {
        width: 110,
        height: 110
    },
    textSection: {
        flex: 1,
        marginTop: 15,
        marginBottom: 15,
        paddingRight: 10
    },
    text: {
        fontFamily: 'Prompt-Medium',
        fontSize: 14
    },
    addNewListSection: {
        flex: 0,
        margin: 10
    }
})

export default NoDataOnSelected;