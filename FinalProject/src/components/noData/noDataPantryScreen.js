import React  from 'react';
import { StyleSheet, View, Image } from 'react-native';
import { Caption, FAB } from 'react-native-paper';
import { useNavigation } from '@react-navigation/native';

const NoDataPantryScreen = () => {
    const navigation = useNavigation();

    return (
        <View style={styles.noDataSection}>
            <View style={styles.imgSection}>
                <Image
                    source={require('../../assets/empty-fridge.png')}
                    style={styles.img}
                />
            </View>
            <View style={styles.textSection}>
                <Caption style={styles.text}>Your pantry is empty</Caption>
            </View>
            <View style={styles.addNewListSection}>
                <FAB
                    style={styles.fab}
                    label='Add new item'
                    small
                    icon="plus"
                    color='#4C4C4C'
                    onPress={() => navigation.navigate('AddItemScreen')}
                />
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    noDataSection: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    imgSection: {
        flex: 0,
        margin: 5
    },
    img: {
        width: 110,
        height: 110
    },
    textSection: {
        flex: 0,
        marginTop: 10,
        marginBottom: 10
    },
    text: {
        fontFamily: 'JosefinSans-Medium',
        fontSize: 15
    },
    addNewListSection: {
        flex: 0,
        margin: 10
    },
    fab: {
        height: 37,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#E1E6F2',
        borderWidth: 2,
        borderColor: '#4481BD'
    }
})

export default NoDataPantryScreen;