import React from 'react';
import { StyleSheet, View, Image } from 'react-native';
import { Caption } from 'react-native-paper';

const NoDataHistoryScreen = () => {
    return (
        <View style={styles.noDataSection}>
            <View style={styles.imgSection}>
                <Image
                    source={require('../../assets/history.png')}
                    style={styles.img}
                />
            </View>
            <View style={styles.textSection}>
                <Caption style={styles.text}>No History</Caption>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    noDataSection: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    imgSection: {
        flex: 0,
        margin: 5
    },
    img: {
        width: 95,
        height: 95
    },
    textSection: {
        flex: 0,
        margin: 5
    },
    text: {
        fontFamily: 'JosefinSans-Medium',
        fontSize: 15
    }
})

export default NoDataHistoryScreen;