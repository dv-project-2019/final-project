import React from 'react';
import { View, StyleSheet, Text } from 'react-native';
import { IconButton } from 'react-native-paper';
import { useNavigation } from '@react-navigation/native';

const MainHeader = ({ title, itemData }) => {
    const navigation = useNavigation();

    const showBackArrow = (title) => {
        if (title === 'Profile' || title === 'Edit Profile'
            || title === 'Edit Profile' || title === 'Add to schedule' || title === 'Edit Item' || title === 'Item Information') {
            return true;
        } else {
            return false;
        }
    }

    const showEditIcon = (title) => {
        if (title === 'Profile' || title === 'Item Information') {
            return true;
        } else {
            return false;
        }
    }

    const checkNavigateEditInfo = (screen) => {
        if (screen === 'Profile') {
            return navigation.navigate('EditProfile');
        } else {
            return navigation.navigate('EditItemInfo', { itemInfo: itemData });
        }
    }

    return (
        <View style={styles.headerRow}>
            <View style={styles.headerStart}>
                {showBackArrow(title) ?
                    <IconButton
                        icon="chevron-left"
                        color='white'
                        size={28}
                        onPress={() => navigation.goBack()}
                    />
                    :
                    <View />

                }
            </View>
            <View style={styles.headerCenter}>
                <Text style={styles.topicText}>{title}</Text>
            </View>
            <View style={styles.headerEnd}>
                {showEditIcon(title) ?
                    <IconButton
                        icon="square-edit-outline"
                        color='white'
                        size={22}
                        style={styles.editIcon}
                        onPress={() => checkNavigateEditInfo(title)}
                    />
                    :
                    <View />
                }
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    headerRow: {
        flex: 0,
        flexDirection: 'row',
        height: 50,
        backgroundColor: '#3D55B6'
    },
    headerStart: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'flex-start'
    },
    headerCenter: {
        flex: 2,
        alignItems: 'center',
        justifyContent: 'center'
    },
    headerEnd: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'flex-end'
    },
    topicText: {
        fontFamily: 'Prompt-Medium',
        fontSize: 16,
        color: 'white'
    },
    bellIcon: {
        margin: 10
    },
    editIcon: {
        margin: 8
    }

})

export default MainHeader;
