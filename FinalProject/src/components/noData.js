import React from 'react';
import { StyleSheet, View } from 'react-native';
import NoDaTaPantryScreen from './noData/noDataPantryScreen';
import NoDataHistoryScreen from './noData/noDataHistoryScreen';
import NoDataOnSelected from './noData/noDataOnSelected';

const NoData = ({ screen }) => {
    return (
        <View style={styles.container}>
            {screen === 'All' || screen === 'Fridge' || screen === 'Freezer' || screen === 'Couter'?
                <NoDataOnSelected catergoty={screen}/>
                :
                screen === 'Pantry' || screen === 'Add to schedule' ?
                <NoDaTaPantryScreen />
                :
                <NoDataHistoryScreen />
            }
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    }
})

export default NoData;