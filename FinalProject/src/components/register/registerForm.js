import React, { useState, useRef } from 'react';
import { View, StyleSheet, Text } from 'react-native';
import { TextInput, IconButton, Button, ActivityIndicator } from 'react-native-paper';
import DateTimePickerModal from "react-native-modal-datetime-picker";
import { useNavigation } from '@react-navigation/native';
import { dateFormat } from '../helper/dateFormat';
import * as yup from 'yup';
import { Formik } from 'formik';

const RegisterForm = ({ onClickRegister, dateOfBirth, setdateOfBirth}) => {
    const [visible, setVisible] = useState(false);
    const emailInput = useRef();
    const firstnameInput = useRef();
    const lastnameInput = useRef();
    const passwordInput = useRef();
    const confirmPasswordInput = useRef();
    const dobInput = useRef();

    const showDatePicker = () => {
        setVisible(true);
    }

    const hideDatePicker = () => {
        setVisible(false);
    }

    const handleConfirm = (date) => {
        hideDatePicker();
        setdateOfBirth(date.getTime());
    }

    const registerSchema = yup.object().shape({
        email: yup.string()
            .required('Email is required, eg.example@mail.com')
            .email('Invalid email address, please input in email format'),
        firstname: yup.string()
            .required('First name is required'),
        lastname: yup.string()
            .required('Last name is required'),
        password: yup.string()
            .min(4, 'Password must contain at least 4 characters')
            .required('Password is required'),
        confirmPassword: yup.string()
            .min(4, 'Password must contain at least 4 characters')
            .required('Confirm password is required')
            .test('passwords-match', 'Passwords must match', function (value) {
                return this.parent.password === value;
            }),
    });

    return (
        <Formik
            validationSchema={registerSchema}
            initialValues={{
                email: '',
                firstname: '',
                lastname: '',
                password: '',
                confirmPassword: ''
            }}
            onSubmit={(values, actions) => {
                onClickRegister(values);
                setTimeout(() => {
                    actions.setSubmitting(false);
                }, 1000);
            }}
        >
            {({ handleChange, handleSubmit, handleBlur, isValid, values, errors, touched, isSubmitting }) => (
                <View style={styles.profileInfoSection}>
                    <View style={styles.form}>
                        <View style={styles.textInputBlock}>
                            <View style={styles.topic}>
                                <Text style={styles.topicText}>Email</Text>
                            </View>
                            <View style={styles.textInput}>
                                <TextInput
                                    name="email"
                                    mode='outlined'
                                    placeholder='email'
                                    value={values.email}
                                    style={styles.input}
                                    onChangeText={handleChange('email')}
                                    onBlur={handleBlur('email')}
                                    ref={emailInput}
                                    autoCapitalize="none"
                                    returnKeyType="next"
                                    onSubmitEditing={() => firstnameInput.current.focus()}
                                />
                            </View>
                            <View style={styles.validateError}>
                                {errors.email && touched.email ? (
                                    <Text style={styles.errorText}>{errors.email}</Text>
                                ) : null}
                            </View>
                        </View>
                        <View style={styles.textInputBlock}>
                            <View style={styles.topic}>
                                <Text style={styles.topicText}>First Name</Text>
                            </View>
                            <View style={styles.textInput}>
                                <TextInput
                                    name="firstname"
                                    mode='outlined'
                                    placeholder='first name'
                                    value={values.firstname}
                                    style={styles.input}
                                    onChangeText={handleChange('firstname')}
                                    onBlur={handleBlur('firstname')}
                                    ref={firstnameInput}
                                    autoCapitalize="none"
                                    returnKeyType="next"
                                    onSubmitEditing={() => lastnameInput.current.focus()}
                                />
                            </View>
                            <View style={styles.validateError}>
                                {errors.firstname && touched.firstname ? (
                                    <Text style={styles.errorText}>{errors.firstname}</Text>
                                ) : null}
                            </View>
                        </View>
                        <View style={styles.textInputBlock}>
                            <View style={styles.topic}>
                                <Text style={styles.topicText}>Last Name</Text>
                            </View>
                            <View style={styles.textInput}>
                                <TextInput
                                    name="lastname"
                                    mode='outlined'
                                    placeholder='last name'
                                    value={values.lastname}
                                    style={styles.input}
                                    onChangeText={handleChange('lastname')}
                                    onBlur={handleBlur('lastname')}
                                    ref={lastnameInput}
                                    autoCapitalize="none"
                                    returnKeyType="next"
                                    onSubmitEditing={() => passwordInput.current.focus()}
                                />
                            </View>
                            <View style={styles.validateError}>
                                {errors.lastname && touched.lastname ? (
                                    <Text style={styles.errorText}>{errors.lastname}</Text>
                                ) : null}
                            </View>
                        </View>
                        <View style={styles.textInputBlock}>
                            <View style={styles.topic}>
                                <Text style={styles.topicText}>Password</Text>
                            </View>
                            <View style={styles.textInput}>
                                <TextInput
                                    name="password"
                                    mode='outlined'
                                    placeholder='last name'
                                    value={values.password}
                                    style={styles.input}
                                    onChangeText={handleChange('password')}
                                    onBlur={handleBlur('password')}
                                    ref={passwordInput}
                                    autoCapitalize="none"
                                    returnKeyType="next"
                                    secureTextEntry={true}
                                    onSubmitEditing={() => confirmPasswordInput.current.focus()}
                                />
                            </View>
                            <View style={styles.validateError}>
                                {errors.password && touched.password ? (
                                    <Text style={styles.errorText}>{errors.password}</Text>
                                ) : null}
                            </View>
                        </View>
                        <View style={styles.textInputBlock}>
                            <View style={styles.topic}>
                                <Text style={styles.topicText}>Confirm Password</Text>
                            </View>
                            <View style={styles.textInput}>
                                <TextInput
                                    name="confirmPassword"
                                    mode='outlined'
                                    placeholder='last name'
                                    value={values.confirmPassword}
                                    style={styles.input}
                                    onChangeText={handleChange('confirmPassword')}
                                    onBlur={handleBlur('confirmPassword')}
                                    ref={confirmPasswordInput}
                                    autoCapitalize="none"
                                    returnKeyType="next"
                                    secureTextEntry={true}
                                    onSubmitEditing={() => dobInput.current.focus()}
                                />
                            </View>
                            <View style={styles.validateError}>
                                {errors.confirmPassword && touched.confirmPassword ? (
                                    <Text style={styles.errorText}>{errors.confirmPassword}</Text>
                                ) : null}
                            </View>
                        </View>
                        <View style={styles.textInputBlock}>
                            <View style={styles.topic}>
                                <Text style={styles.topicText}>Date Of Birth</Text>
                            </View>
                            <View style={styles.rowBlock}>
                                <View style={styles.dateInput}>
                                    <TextInput
                                        mode='outlined'
                                        placeholder='Date of birth'
                                        value={dateFormat(dateOfBirth)}
                                        style={styles.input}
                                        ref={dobInput}
                                        autoCapitalize="none"
                                        returnKeyType="next"
                                    />
                                </View>
                                <View style={styles.dateIcon}>
                                    <DateTimePickerModal
                                        isVisible={visible}
                                        mode="date"
                                        onConfirm={handleConfirm}
                                        onCancel={hideDatePicker}
                                    />
                                    <IconButton
                                        icon='calendar-month'
                                        size={23}
                                        color='#18467F'
                                        onPress={() => showDatePicker()}
                                    />
                                </View>
                            </View>
                        </View>

                        <View style={styles.buttonBlock}>
                            {isSubmitting ? (
                                <ActivityIndicator size="small" color="#3FC6DD"></ActivityIndicator>
                            ) : (
                                    <Button
                                        mode="contained"
                                        uppercase={false}
                                        onPress={handleSubmit}
                                        style={styles.addButton}
                                    >
                                        Create Account
                                    </Button>
                                )}

                        </View>
                    </View>
                </View>
            )}
        </Formik>

    )
}

const styles = StyleSheet.create({
    profileInfoSection: {
        flex: 1,
        backgroundColor: '#F9F9F9'
    },
    form: {
        flex: 0,
        margin: 15
    },
    textInputBlock: {
        flex: 1,
        marginLeft: 20,
        marginRight: 20,
        marginTop: 5,
        marginBottom: 15
    },
    topic: {
        marginTop: 5
    },
    topicText: {
        fontFamily: 'Prompt-SemiBold',
        fontSize: 16,
        color: '#323232'
    },
    input: {
        height: 48,
        backgroundColor: '#F9F9F9'
    },
    rowBlock: {
        flex: 1,
        flexDirection: 'row'
    },
    dateInput: {
        flex: 1
    },
    dateIcon: {
        flex: 0,
        justifyContent: 'center'
    },
    buttonBlock: {
        flex: 0,
        marginTop: 15,
        marginBottom: 15,
        padding: 10,
        justifyContent: 'center',
        alignItems: 'center'
    },
    addButton: {
        width: 240,
        height: 48,
        justifyContent: 'center',
        backgroundColor: '#4B80D4'
    },
    validateError: {
        flex: 0,
        marginLeft: 5,
        marginRight: 5,
        marginTop: 2,
        marginBottom: 2
    },
    errorText: {
        fontFamily: 'Prompt-Medium',
        flex: 0,
        color: 'red',
        fontSize: 12
    }
})

export default RegisterForm;