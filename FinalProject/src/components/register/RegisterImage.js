import React from 'react';
import { View, StyleSheet, TouchableHighlight } from 'react-native';
import { Avatar } from 'react-native-paper';

const RegisterImage = ({ profileImg }) => {
    return (
        <View style={styles.profileImgSection}>
            
            <View style={styles.imgBlock}>
                <Avatar.Image
                    size={150}
                    source={
                        profileImg === '' ?
                            { uri: 'https://upload.wikimedia.org/wikipedia/commons/thumb/7/7e/Circle-icons-profile.svg/768px-Circle-icons-profile.svg.png' }
                            :
                            { uri: profileImg }
                    }
                />
            </View>
            <View style={styles.cameraIconBlock}>
                <TouchableHighlight
                    activeOpacity={0.8}
                    underlayColor="#4B80D4"
                    onPress={() => alert('upload')}
                    style={styles.touchHighlight}
                >
                    <Avatar.Icon
                        size={38}
                        icon="camera"
                        color="#18467F"
                        style={styles.cameraIcon}
                    />
                </TouchableHighlight>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    profileImgSection: {
        flex: 0,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#3D55B6'
    },
    imgBlock: {
        flex: 0,
        margin: 35
    },
    cameraIconBlock: {
        position: 'absolute',
        right: 122,
        top: 40
    },
    cameraIcon: {
        backgroundColor: '#F1F1F1'
    },
    touchHighlight: {
        borderRadius: 100
    }
})

export default RegisterImage;