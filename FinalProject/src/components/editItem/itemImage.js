import React, { useState } from 'react';
import { View, StyleSheet, Image } from 'react-native';
import { FAB } from 'react-native-paper';
import { host } from '../../api';

const ItemImage = ({ itemId, itemImg }) => {
    const [image, setImage] = useState(host + "/image/" + itemId + "/" + itemImg.filename);
    
    return (
        <View style={styles.profileImgSection}>
            <Image
                source={
                    itemImg === undefined || itemImg.filename == "" ?
                        { uri: 'https://img.icons8.com/clouds/2x/ingredients.png' }
                        :
                        { uri: image }
                }
                style={
                    itemImg === undefined || itemImg.filename === ""?
                    styles.imageIcon
                    :
                    styles.image
                }
            />
        </View>
    )
}

const styles = StyleSheet.create({
    profileImgSection: {
        flex: 0,
        backgroundColor: '#E1E6F2',
        justifyContent: 'center',
        alignItems: 'center',
        height: 230
    },
    image: {
        height: 230,
        width: '100%'
    },
    imageIcon: {
        height: 200,
        width: 200,
    },
    fab: {
        position: "absolute",
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#E4ECFB',
        borderColor: '#C0C0C0',
        borderWidth: 1,
        height: 40,
        width: 116
    }
})

export default ItemImage;
