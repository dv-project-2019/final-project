import React, { useState, useRef } from 'react';
import { View, StyleSheet, Text, TouchableOpacity, Alert } from 'react-native';
import { IconButton, TextInput, RadioButton, Button, ActivityIndicator } from 'react-native-paper';
import DateTimePickerModal from "react-native-modal-datetime-picker";
import { dateFormat } from '../helper/dateFormat';
import { connect } from 'react-redux';
import { useNavigation } from '@react-navigation/native';
import { updateItemInfo } from '../../api';
import * as yup from 'yup';
import { Formik } from 'formik';

const ItemForm = ({ itemInformation, token }) => {
    const [visible, setVisible] = useState(false);
    const itemnameInput = useRef();
    const caloriesInput = useRef();
    const itemDescriptInput = useRef();
    const amountInput = useRef();
    const unitInput = useRef();
    const expirationInput = useRef();
    const navigation = useNavigation();
    const itemId = itemInformation.id;
    const [itemInfo, setItemInfo] = useState({
        itemName: itemInformation.itemName,
        description: itemInformation.description,
        calorie: itemInformation.calorie,
        amount: itemInformation.amount,
        unit: itemInformation.unit,
        category: itemInformation.category,
        expiration_date: itemInformation.expiration_date
    })

    const checkRedioButtonSelected = (radioButtonValue) => {
        if (radioButtonValue === itemInformation.category) {
            return styles.radioValueTextSelected
        } else {
            return styles.radioValueText
        }
    }

    const showDatePicker = () => {
        setVisible(true);
    }

    const hideDatePicker = () => {
        setVisible(false);
    }

    const handleConfirm = (date) => {
        hideDatePicker();
        setItemInfo({
            ...itemInfo,
            expiration_date: date.getTime()
        });
    }

    const onClickUpdate = (formValue) => {
        updateItemInfo(formValue, itemInfo, itemId, token)
            .then(() => {
                navigation.goBack();
            })
            .catch((err) => {
                console.log(err);
                setTimeout(() => {
                    Alert.alert("Error", "Update item information is not success");
                }, 1000);
            })
    }

    const addItemSchema = yup.object().shape({
        itemName: yup.string()
            .required('Item name is required'),
        unit: yup.string()
            .required('Unit is required'),
        calorie: yup.number()
            .integer('Please input number')
            .positive('Input should be positive number')
            .required('Calories is required'),
        amount: yup.number()
            .integer('Please input number')
            .positive('Value should be positive number')
            .required('Amount is required'),
    });

    return (
        <Formik
            validationSchema={addItemSchema}
            initialValues={{
                itemName: itemInfo.itemName,
                description: itemInfo.description,
                calorie: itemInfo.calorie,
                amount: itemInfo.amount,
                unit: itemInfo.unit
            }}
            enableReinitialize={false}
            onSubmit={(values, actions) => {
                onClickUpdate(values);
                setTimeout(() => {
                    actions.setSubmitting(false);
                }, 1000);
            }}
        >
            {({ handleChange, handleSubmit, handleBlur, values, errors, touched, isSubmitting }) => (
                <View style={styles.itemFormSection}>
                    <View style={styles.textInputSection}>
                        <View style={styles.textInputBlock}>
                            <View style={styles.topic}>
                                <Text style={styles.topicText}>Item Name</Text>
                            </View>
                            <View style={styles.textInput}>
                                <TextInput
                                    name="itemName"
                                    mode='outlined'
                                    placeholder='Item name'
                                    value={values.itemName}
                                    style={styles.input}
                                    onChangeText={handleChange('itemName')}
                                    onBlur={handleBlur('itemName')}
                                    ref={itemnameInput}
                                    autoCapitalize="none"
                                    returnKeyType="next"
                                    onSubmitEditing={() => itemDescriptInput.current.focus()}
                                />
                            </View>
                            <View style={styles.validateError}>
                                {errors.itemName && touched.itemName ? (
                                    <Text style={styles.errorText}>{errors.itemName}</Text>
                                ) : null}
                            </View>
                        </View>

                        <View style={styles.textInputBlock}>
                            <View style={styles.topic}>
                                <Text style={styles.topicText}>Item Description</Text>
                            </View>
                            <View style={styles.textInput}>
                                <TextInput
                                    name="description"
                                    mode='outlined'
                                    multiline={true}
                                    placeholder='Description'
                                    value={values.description}
                                    onChangeText={handleChange('description')}
                                    onBlur={handleBlur('description')}
                                    blurOnSubmit={true}
                                    maxLength={150}
                                    numberOfLines={3}
                                    ref={itemDescriptInput}
                                    autoCapitalize="none"
                                    returnKeyType="next"
                                    onSubmitEditing={() => caloriesInput.current.focus()}
                                />
                            </View>
                            <View style={styles.validateError}>
                                {errors.description && touched.description ? (
                                    <Text style={styles.errorText}>{errors.description}</Text>
                                ) : null}
                            </View>
                        </View>

                        <View style={styles.textInputBlock}>
                            <View style={styles.topic}>
                                <Text style={styles.topicText}>Calories</Text>
                            </View>
                            <View style={styles.textInput}>
                                <TextInput
                                    name="calorie"
                                    mode='outlined'
                                    placeholder='Calories'
                                    value={values.calorie.toString()}
                                    style={styles.input}
                                    onChangeText={handleChange('calorie')}
                                    onBlur={handleBlur('calorie')}
                                    ref={caloriesInput}
                                    autoCapitalize="none"
                                    returnKeyType="next"
                                    keyboardType="number-pad"
                                    onSubmitEditing={() => amountInput.current.focus()}
                                />
                            </View>
                            <View style={styles.validateError}>
                                {errors.calorie && touched.calorie ? (
                                    <Text style={styles.errorText}>{errors.calorie}</Text>
                                ) : null}
                            </View>
                        </View>

                        <View style={styles.amountTextInput}>
                            <View style={styles.amountInputBlock}>
                                <View style={styles.topic}>
                                    <Text style={styles.topicText}>Amount</Text>
                                </View>
                                <View style={styles.amountInput}>
                                    <TextInput
                                        name="amount"
                                        mode='outlined'
                                        placeholder='Amount'
                                        value={values.amount.toString()}
                                        style={styles.input}
                                        onChangeText={handleChange('amount')}
                                        onBlur={handleBlur('amount')}
                                        ref={amountInput}
                                        autoCapitalize="none"
                                        returnKeyType="next"
                                        keyboardType="number-pad"
                                        onSubmitEditing={() => unitInput.current.focus()}
                                    />
                                </View>
                                <View style={styles.validateError}>
                                    {errors.amount && touched.amount ? (
                                        <Text style={styles.errorText}>{errors.amount}</Text>
                                    ) : null}
                                </View>
                            </View>

                            <View style={styles.unitsInputBlock}>
                                <View style={styles.topic}>
                                    <Text style={styles.topicText}>Unit</Text>
                                </View>
                                <View style={styles.unitsInput}>
                                    <TextInput
                                        name="unit"
                                        mode='outlined'
                                        placeholder='e.g. kg, etc.'
                                        value={values.unit}
                                        style={styles.input}
                                        onChangeText={handleChange('unit')}
                                        onBlur={handleBlur('unit')}
                                        ref={unitInput}
                                        autoCapitalize="none"
                                        returnKeyType="next"
                                        onSubmitEditing={() => expirationInput.current.focus()}
                                    />
                                </View>
                                <View style={styles.validateError}>
                                    {errors.unit && touched.unit ? (
                                        <Text style={styles.errorText}>{errors.unit}</Text>
                                    ) : null}
                                </View>
                            </View>
                        </View>

                        <View style={styles.textInputBlock}>
                            <View style={styles.topic}>
                                <Text style={styles.topicText}>Expiration Date</Text>
                            </View>
                            <View style={styles.rowBlock}>
                                <View style={styles.dateInput}>
                                    <TextInput
                                        mode='outlined'
                                        placeholder='Expiration date'
                                        value={dateFormat(itemInfo.expiration_date)}
                                        style={styles.input}
                                        ref={expirationInput}
                                        autoCapitalize="none"
                                        returnKeyType="next"
                                    />
                                </View>
                                <View style={styles.dateIcon}>
                                    <DateTimePickerModal
                                        isVisible={visible}
                                        mode="date"
                                        onConfirm={handleConfirm}
                                        onCancel={hideDatePicker}
                                    />
                                    <IconButton
                                        icon='calendar-month'
                                        size={23}
                                        color='#18467F'
                                        onPress={() => showDatePicker()}
                                    />
                                </View>
                            </View>
                        </View>

                        <View style={styles.textInputBlock}>
                            <View style={styles.topic}>
                                <Text style={styles.topicText}>Location Category</Text>
                            </View>
                            <View style={styles.select}>
                                <RadioButton.Group
                                    onValueChange={value => setItemInfo({
                                        ...itemInfo,
                                        category: value
                                    })
                                    }
                                    value={itemInfo.category}
                                >
                                    <View style={styles.rowBlock}>
                                        <View style={styles.radioButton}>
                                            <RadioButton
                                                value="Fridge"
                                                color='#18467F'
                                            />
                                        </View>
                                        <View style={styles.radioValue}>
                                            <Text style={checkRedioButtonSelected('Fridge')}>Fridge</Text>
                                        </View>
                                    </View>
                                    <View style={styles.rowBlock}>
                                        <View style={styles.radioButton}>
                                            <RadioButton
                                                value="Freezer"
                                                color='#18467F'
                                            />
                                        </View>
                                        <View style={styles.radioValue}>
                                            <Text style={checkRedioButtonSelected('Freezer')}>Freezer</Text>
                                        </View>
                                    </View>
                                    <View style={styles.rowBlock}>
                                        <View style={styles.radioButton}>
                                            <RadioButton
                                                value="Pantry"
                                                color='#18467F'
                                            />
                                        </View>
                                        <View style={styles.radioValue}>
                                            <Text style={checkRedioButtonSelected('Pantry')}>Pantry</Text>
                                        </View>
                                    </View>
                                </RadioButton.Group>
                            </View>
                        </View>
                        <View style={styles.buttonBlock}>
                            {isSubmitting ? (
                                <ActivityIndicator size="small" color="#3FC6DD"></ActivityIndicator>
                            ) : (
                                    <Button
                                        mode="contained"
                                        uppercase={false}
                                        onPress={handleSubmit}
                                        style={styles.addButton}
                                    >
                                        Update
                                    </Button>
                                )}
                        </View>

                    </View>
                </View>
            )
            }
        </Formik >
    )
}

const styles = StyleSheet.create({
    itemFormSection: {
        flex: 1,
        backgroundColor: '#F9F9F9'
    },
    textInputBlock: {
        flex: 1,
        marginLeft: 20,
        marginRight: 20,
        marginTop: 10,
        marginBottom: 10
    },
    textInput: {
        flex: 0
    },
    input: {
        height: 48
    },
    textInputSection: {
        flex: 0,
        margin: 15
    },
    amountTextInput: {
        flex: 0,
        flexDirection: 'row',
        marginTop: 10,
        marginBottom: 10
    },
    amountInput: {
        flex: 1
    },
    unitsInput: {
        flex: 1
    },
    topic: {
        marginTop: 5
    },
    topicText: {
        fontFamily: 'Prompt-SemiBold',
        fontSize: 16,
        color: '#323232'
    },
    amountInputBlock: {
        flex: 2,
        marginLeft: 20,
        marginRight: 5
    },
    unitsInputBlock: {
        flex: 1,
        marginLeft: 5,
        marginRight: 20
    },
    select: {
        flex: 0,
        flexDirection: 'row',
        marginTop: 5
    },
    rowBlock: {
        flex: 1,
        flexDirection: 'row'
    },
    radioButton: {
        flex: 0,
        justifyContent: 'center'
    },
    radioValue: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'flex-start'
    },
    radioValueText: {
        fontFamily: 'Prompt-Medium',
        fontSize: 15,
        color: '#323232'
    },
    radioValueTextSelected: {
        fontFamily: 'Prompt-Medium',
        fontSize: 15,
        color: '#18467F'
    },
    dateInput: {
        flex: 1
    },
    dateIcon: {
        flex: 0,
        justifyContent: 'center'
    },
    buttonBlock: {
        flex: 0,
        marginTop: 15,
        marginBottom: 15,
        padding: 10,
        justifyContent: 'center',
        alignItems: 'center'
    },
    addButton: {
        width: '40%',
        backgroundColor: '#4B80D4'
    },
    validateError: {
        flex: 0,
        marginLeft: 5,
        marginRight: 5,
        marginTop: 2,
        marginBottom: 2
    },
    errorText: {
        fontFamily: 'Prompt-Medium',
        flex: 0,
        color: 'red',
        fontSize: 12
    }
})

const mapStateToProps = (state) => {
    return {
        token: state.user.token
    }
}

export default connect(mapStateToProps, null)(ItemForm);