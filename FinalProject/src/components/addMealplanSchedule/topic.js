import React, { useState } from 'react';
import { StyleSheet, View, Text, Image } from 'react-native';
import moment from 'moment';
import { checkWeatherImg } from '../helper/weatherImage';

const Topic = ({time}) => {
    const [ currentDate ] = useState(moment().format('dddd LL'));

    return (
        <View style={styles.dateTopic}>
            <View style={styles.mainTopic}>
                <View style={styles.topic}>
                <Text style={styles.topicText}>{time}</Text>
            </View>
            <View style={styles.icon}>
                <Image source={checkWeatherImg(time)} style={styles.iconImage} />
            </View>
            </View>
            
            <View style={styles.date}>
                <Text style={styles.dateText}>{currentDate}</Text>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    dateTopic: {
        flex: 0,
        padding: 10,
        backgroundColor: '#FFFFFF',
        borderBottomWidth: 1,
        borderBottomColor: '#DEDEDE'
    },
    mainTopic: {
        flex: 0,
        flexDirection: 'row',
        marginLeft: 5,
        marginRight: 5,
        marginTop: 10,
        marginBottom: 5
    },
    topic: {
        flex: 0
    },
    topicText: {
        fontFamily: 'Prompt-SemiBold',
        fontSize: 22,
        color: '#323232'
    },
    icon: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'flex-start',
        marginLeft: 10
    },
    iconImage: {
        width: 32,
        height: 32
    },
    date: {
        flex: 0,
        marginLeft: 5,
        marginRight: 5,
        marginTop: 5,
        marginBottom: 10
    },
    dateText: {
        fontFamily: 'Prompt-Medium',
        fontSize: 18,
        color: '#415F85'
    },
    
})

export default Topic;