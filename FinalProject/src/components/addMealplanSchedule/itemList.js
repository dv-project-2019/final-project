import React from 'react';
import { StyleSheet, View, Text, Image, SafeAreaView, FlatList, Alert } from 'react-native';
import { Card, IconButton } from 'react-native-paper';
import NoData from '../noData';
import { dateFormat } from '../helper/dateFormat';
import { connect } from 'react-redux';
import { host, addItemToMoningplan, addItemToAfternoonplan, addItemToEveningplan } from '../../api';

const ItemList = ({ title, itemsList, mealplanId, time, timeId, token, refreshItem }) => {

    const checkEmptyData = (data) => {
        if (data.length === 0) {
            return true;
        } else {
            return false;
        }
    }

    const SuccessAlert = () => {
        setTimeout(() => {
            refreshItem();
            Alert.alert("Success", "Item is added");
        }, 200);
    }

    const ErrorAlert = () => {
        setTimeout(() => {
            refreshItem();
            Alert.alert("Error", "Cannot added, please try again");
        }, 200);
    }

    const selectItem = (mealplanId, itemId, timeName, timeId) => {
        if (timeName === "Morning") {
            addItemToMoningplan(token, mealplanId, itemId, timeId)
                .then(() => {
                    SuccessAlert();
                })
                .catch((err) => {
                    console.log(err);
                    ErrorAlert();
                })
        } else if (timeName === "Afternoon") {
            addItemToAfternoonplan(token, mealplanId, itemId, timeId)
                .then(() => {
                    SuccessAlert();
                })
                .catch((err) => {
                    console.log(err);
                    ErrorAlert();
                })
        } else{
            addItemToEveningplan(token, mealplanId, itemId, timeId)
                .then(() => {
                    SuccessAlert();
                })
                .catch((err) => {
                    console.log(err);
                    ErrorAlert();
                })
        }
    }

    const Item = (list) => {
        return (
            <Card style={styles.card}>
                <View style={styles.cardContent}>
                    <View style={styles.itemImgBlock}>
                        <Image
                            source={list.image.filename === "" ?
                                { uri: 'https://img.icons8.com/clouds/2x/ingredients.png' }
                                :
                                { uri: host + "/image/" + list.id + "/" + list.image.filename }
                            }
                            style={styles.itemImg}
                        />
                    </View>
                    <View style={styles.itemData}>
                        <View style={styles.itemName}>
                            <Text style={styles.itemNameText}>{list.itemName}</Text>
                        </View>
                        <View style={styles.itemName}>
                            <Text style={styles.itemCalText}>{list.calorie} Calories</Text>
                        </View>
                        <View style={styles.itemAmount}>
                            <View style={styles.icon}>
                                <IconButton
                                    icon='scale'
                                    size={11}
                                    style={styles.iconImg}
                                    color='#414141'
                                />
                            </View>
                            <View style={styles.amount}>
                                <Text style={styles.amountText}>{list.amount}</Text>
                            </View>
                            <View style={styles.unit}>
                                <Text style={styles.amountText}>{list.unit}</Text>
                            </View>
                        </View>
                        <View style={styles.category}>
                            <View style={styles.icon}>
                                <IconButton
                                    icon='snowflake'
                                    size={11}
                                    style={styles.iconImg}
                                    color='#414141'
                                />
                            </View>
                            <View style={styles.amount}>
                                <Text style={styles.amountText}>{list.category}</Text>
                            </View>
                        </View>

                        {list.isExpired ?
                            <View style={styles.expiration}>
                                <View style={styles.expirationIcon}>
                                    <IconButton
                                        icon='clock'
                                        size={11}
                                        style={styles.iconImg}
                                        color='#C62020'
                                    />
                                </View>
                                <View style={styles.expirationDate}>
                                    <Text style={styles.expiredText}>{dateFormat(list.expiration_date)}</Text>
                                </View>
                            </View>
                            :
                            <View style={styles.expiration}>
                                <View style={styles.expirationIcon}>
                                    <IconButton
                                        icon='clock'
                                        size={11}
                                        style={styles.iconImg}
                                        color='#239B56'
                                    />
                                </View>
                                <View style={styles.expirationDate}>
                                    <Text style={styles.expirationText}>{dateFormat(list.expiration_date)}</Text>
                                </View>
                            </View>
                        }
                    </View>
                    <View style={styles.button}>
                        {list.isSelected ?
                            <IconButton
                                icon='check-circle-outline'
                                size={35}
                                color='#4B80D4'
                            />
                            :
                            <IconButton
                                icon='plus-circle-outline'
                                size={35} color='#808080'
                                onPress={() => selectItem(mealplanId, list.id, time, timeId)}
                            />
                        }

                    </View>
                </View>
            </Card>
        )
    }

    return (
        <View style={styles.listSection}>
            {checkEmptyData(itemsList) ?
                <NoData screen={title} />
                :
                <SafeAreaView style={styles.container}>
                    <FlatList
                        data={itemsList}
                        renderItem={({ item }) => Item(item)}
                        keyExtractor={(item, index) => index.toString()}
                    />
                </SafeAreaView>
            }

        </View>
    )
}

const styles = StyleSheet.create({
    listSection: {
        flex: 1,
        backgroundColor: '#F9F9F9',
        paddingLeft: 5,
        paddingRight: 5,
        paddingBottom: 5
    },
    container: {
        flex: 1
    },
    card: {
        flex: 1,
        backgroundColor: 'white',
        marginLeft: 5,
        marginRight: 5,
        marginTop: 5,
    },
    cardContent: {
        flex: 0,
        flexDirection: 'row'
    },
    itemImgBlock: {
        flex: 0,
        margin: 10
    },
    itemData: {
        flex: 1,
        marginLeft: 10,
        marginRight: 10,
        marginBottom: 8,
        marginTop: 5
    },
    itemImg: {
        width: 112,
        height: 112
    },
    nameRow: {
        flex: 1,
        flexDirection: 'row'
    },
    itemName: {
        flex: 0,
        marginBottom: 8
    },
    itemNameText: {
        fontFamily: 'Prompt-SemiBold',
        fontSize: 14,
        color: '#323232'
    },
    itemCalText: {
        fontFamily: 'Prompt-Medium',
        fontSize: 11,
        color: '#414141'
    },
    itemAmount: {
        flex: 0,
        flexDirection: 'row',
        marginBottom: 8
    },
    icon: {
        paddingRight: 5,
        flex: 0,
        justifyContent: 'center'
    },
    amount: {
        flex: 0,
        justifyContent: 'center'
    },
    unit: {
        flex: 0,
        paddingLeft: 4,
        justifyContent: 'center'
    },
    amountText: {
        fontFamily: 'Prompt-Regular',
        fontSize: 11,
        color: '#414141'
    },
    category: {
        flex: 0,
        flexDirection: 'row',
        marginBottom: 8
    },
    iconImg: {
        width: 11,
        height: 11,
        margin: 0
    },
    expiration: {
        flex: 0,
        flexDirection: 'row'
    },
    expirationIcon: {
        paddingRight: 5,
        flex: 0,
        justifyContent: 'center'
    },
    expirationDate: {
        flex: 0,
        justifyContent: 'center'
    },
    expirationText: {
        fontFamily: 'Prompt-Regular',
        fontSize: 11,
        color: '#414141'
    },
    expiredText: {
        fontFamily: 'Prompt-Regular',
        fontSize: 11,
        color: '#C62020'
    },
    button: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'flex-end'
    }
})

const mapStateToProps = (state) => {
    return {
        token: state.user.token
    }
}


export default connect(mapStateToProps, null)(ItemList);