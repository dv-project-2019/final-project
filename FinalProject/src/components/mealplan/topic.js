import React, { useState } from 'react';
import { StyleSheet, View, Text } from 'react-native';
import moment from 'moment';

const Topic = ({ date }) => {
    const [ currentDate ] = useState(moment().format('dddd LL'));

    return (
        <View style={styles.dateTopic}>
            <View style={styles.topic}>
                <Text style={styles.topicText}>My Plan</Text>
            </View>
            <View style={styles.date}>
                <Text style={styles.dateText}>{currentDate}</Text>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    dateTopic: {
        flex: 0,
        padding: 10,
        backgroundColor: '#FFFFFF',
        borderBottomWidth: 1,
        borderBottomColor: '#DEDEDE'
    },
    topic: {
        flex: 0,
        marginLeft: 5,
        marginRight: 5,
        marginTop: 10,
        marginBottom: 5
    },
    topicText: {
        fontFamily: 'Prompt-SemiBold',
        fontSize: 22,
        color: '#323232'
    },
    date: {
        flex: 0,
        marginLeft: 5,
        marginRight: 5,
        marginTop: 5,
        marginBottom: 10
    },
    dateText: {
        fontFamily: 'Prompt-Medium',
        fontSize: 18,
        color: '#415F85'
    }
})

export default Topic;