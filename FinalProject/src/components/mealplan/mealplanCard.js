import React from 'react';
import { StyleSheet, View, Text, Image, SafeAreaView, FlatList, TouchableHighlight } from 'react-native';
import { Card } from 'react-native-paper';
import IconButton from 'react-native-vector-icons/Feather';
import { useNavigation } from '@react-navigation/native';
import { checkWeatherImg } from '../helper/weatherImage';
import { host } from '../../api';

const MealPlanCard = ({ mealplan }) => {
    const navigation = useNavigation();

    const sumCalories = (list, name) => {
        switch (name) {
            case 'Morning':
                let sumCalMorning = 0;
                list.map((item) => {
                    sumCalMorning += item.calorie
                })
                return sumCalMorning;
            case 'Afternoon':
                let sumCalAfternoon = 0;
                list.map((item) => {
                    sumCalAfternoon += item.calorie
                })
                return sumCalAfternoon;
            case 'Evening':
                let sumCalEvening = 0;
                list.map((item) => {
                    sumCalEvening += item.calorie
                })
                return sumCalEvening;
            default:
                return 0;
        }
    }

    const Item = ({ item }) => {
        return (
            <View style={styles.itemList} >
                <View style={styles.imgBlock}>
                    <Image source={item.image.filename === "" ?
                        { uri: 'https://img.icons8.com/clouds/2x/ingredients.png' }
                        :
                        { uri: host + "/image/" + item.id + "/" + item.image.filename }}
                        style={styles.itemImg} />
                </View>
                <View style={styles.nameBlock}>
                    <Text style={styles.itemName}>{item.itemName}</Text>
                </View>
            </View>
        )
    }

    const ShowAddItem = (time, timeId) => {
        return (
            <View style={styles.itemList} >
                <View style={styles.iconBlock}>
                    <TouchableHighlight
                        activeOpacity={0.9}
                        underlayColor="#E1E6F2"
                        onPress={() => navigation.navigate('AddMealPlanSchedule', {
                            time: time,
                            mealplanId: mealplan.id,
                            timeId: timeId
                        })}
                        style={styles.touchHighlight}
                    >
                        <IconButton
                            name='plus-circle'
                            color='#4481BD'
                            size={56}
                        />
                    </TouchableHighlight>
                </View>

                <View style={styles.nameBlock}>
                    <Text style={styles.itemName}></Text>
                </View>

            </View>
        )
    }

    const MealPlanCard = (meal) => {
        return (
            <View style={styles.cardArea}>
                <Card>
                    <Card.Title
                        title={meal.name}
                        left={(props) =>
                            <Image
                                {...props}
                                source={checkWeatherImg(meal.name)}
                                style={styles.iconImage}
                            />
                        }
                        leftStyle={styles.leftImg}
                        titleStyle={styles.title}
                    />
                    <Card.Content style={styles.content}>
                        <SafeAreaView style={styles.container}>
                            <FlatList
                                data={meal.items}
                                renderItem={(list) => Item(list)}
                                keyExtractor={(item, index) => index.toString()}
                                horizontal={true}
                                ListHeaderComponent={() => ShowAddItem(meal.name, meal.id)}
                            />
                        </SafeAreaView>
                    </Card.Content>
                    <Card.Actions style={styles.cardBottom}>
                        <Text style={styles.calText}>{sumCalories(meal.items, meal.name)} Calories</Text>
                    </Card.Actions>
                </Card>
            </View>
        )
    }

    return (
        <View style={styles.mealplanCard} >
            <SafeAreaView style={styles.container}>
                <FlatList
                    data={mealplan.time}
                    renderItem={({ item }) => MealPlanCard(item)}
                    keyExtractor={(item, index) => index.toString()}
                />
            </SafeAreaView>
        </View>
    )
}

const styles = StyleSheet.create({
    mealplanCard: {
        flex: 1
    },
    container: {
        flex: 1
    },
    cardArea: {
        margin: 10,
        padding: 5
    },
    iconImage: {
        width: 28,
        height: 28
    },
    title: {
        fontFamily: 'Prompt-Medium',
        fontSize: 16,
        color: '#323232',
    },
    leftImg: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    content: {
        flex: 0,
        justifyContent: 'center',
        alignItems: 'flex-start',
        marginLeft: 5,
        marginRight: 5
    },
    itemList: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        margin: 8
    },
    imgBlock: {
        flex: 0
    },
    itemImg: {
        width: 48,
        height: 48,
        borderRadius: 100,
        backgroundColor: '#3FC6DD'
    },
    nameBlock: {
        flex: 0,
        margin: 2
    },
    itemName: {
        fontFamily: 'Prompt-Medium',
        fontSize: 13,
        color: '#323232',
    },
    cardBottom: {
        flex: 0,
        margin: 10,
        alignItems: 'center',
        justifyContent: 'center'
    },
    calText: {
        fontFamily: 'Prompt-Medium',
        fontSize: 12,
        color: '#4B80D4',
    },
    iconBlock: {
        flex: 0
    },
    touchHighlight: {
        borderRadius: 100
    }
})

export default MealPlanCard;