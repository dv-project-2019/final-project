import React from 'react';
import { View, StyleSheet } from 'react-native';
import { PieChart } from "react-native-chart-kit";
import { Dimensions } from "react-native";
import { Card } from 'react-native-paper';

const PieChartSction = ({ data }) => {
    const screenWidth = Dimensions.get("window").width;

    const chartConfig = {
        backgroundGradientFrom: "#1E2923",
        backgroundGradientFromOpacity: 0,
        backgroundGradientTo: "#08130D",
        backgroundGradientToOpacity: 0.5,
        color: (opacity = 1) => `rgba(26, 255, 146, ${opacity})`,
        strokeWidth: 2,
        barPercentage: 0.5
    };

    return (
        <View style={styles.pieChartSection}>
            <Card style={styles.pieChartCard}>
                 <PieChart
                data={data}
                width={screenWidth}
                height={260}
                chartConfig={chartConfig}
                accessor="population"
                backgroundColor="none"
                paddingLeft="80"
                absolute
                hasLegend={false}
            />
            </Card>
           
        </View>
    )
}

const styles = StyleSheet.create({
    pieChartSection: {
        flex: 0,
        justifyContent: 'center'
    },
    pieChartCard: {
        margin: 15
    }
})

export default PieChartSction;