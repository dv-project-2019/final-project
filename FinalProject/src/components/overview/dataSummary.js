import React from 'react';
import { View, StyleSheet, Text } from 'react-native';
import { Title, Card } from 'react-native-paper';

const DataSummary = ({ data }) => {
    const cardStyle = (name) => {
        if(name === 'Expired'){
            return styles.cardExpired
        }else if(name === 'On time'){
            return styles.cardOntime
        }else{
            return styles.cardNotExpired
        }
    }

    return (
        <View style={styles.dataSection}>
            <View style={styles.topic}>
                <Text style={styles.topicText}>Summary</Text>
            </View>
            <View style={styles.dataSummary}>
                {data.map((item, index) => {
                    return (
                            <Card style={cardStyle(item.name)} key={index}>
                                <Card.Title 
                                    title={item.name} 
                                    titleStyle={styles.title}
                                />
                                <Card.Content style={styles.content}>
                                    <Title style={styles.dataText}>{item.population}%</Title>
                                </Card.Content>
                            </Card>
                    )
                })}
            </View>

        </View>
    )
}

const styles = StyleSheet.create({
    dataSection: {
        flex: 0,
        marginLeft: 15,
        marginRight: 15,
        marginBottom: 10
    },
    topic: {
        flex: 0,
        alignItems: 'flex-start',
        paddingTop: 8,
        paddingBottom: 8
    },
    topicText: {
        fontFamily: 'Prompt-SemiBold',
        fontSize: 16,
        color: '#323232'
    },
    dataSummary: {
        flex: 0,
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: 8
    },
    dataText: {
        fontFamily: 'Prompt-Medium',
        color: '#3A3A3A'
    },
    cardExpired :{
       flex: 0,
       borderLeftWidth: 4,
       borderLeftColor: '#EC788F'
    },
    cardOntime :{
        flex: 0,
        borderLeftWidth: 4,
        borderLeftColor: '#88ECA8'
     },
     cardNotExpired :{
        flex: 0,
        borderLeftWidth: 4,
        borderLeftColor: '#6EB6E9'
     },
    title: {
        fontSize: 13, 
        fontFamily: 'Prompt-Medium',
        color: '#777777'
    },
    content: {
        flex: 0,
        width: 100,
        justifyContent: 'center',
        alignItems: 'center',
       marginLeft: 6,
       marginRight: 6
    }
})

export default DataSummary;