import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import AddItem from '../screens/AddItem';

const Stack = createStackNavigator();

const AddItemScreen = () => {
    return (
        <Stack.Navigator
            initialRouteName="AddItem"
            screenOptions={{
                headerShown: false
            }}
        >
            <Stack.Screen
                name="AddItem"
                component={AddItem}
            />
        </Stack.Navigator>
    )
}

export default AddItemScreen;