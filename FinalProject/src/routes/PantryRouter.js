import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import Pantry from '../screens/Pantry';
import ItemInfo from '../screens/ItemInfo';
import EditItemInfo from '../screens/EditItem';

const Stack = createStackNavigator();

const PantryScreen = () => {
    return (
        <Stack.Navigator
            initialRouteName="Pantry"
            screenOptions={{
                headerShown: false
            }}
        >
            <Stack.Screen
                name="Pantry"
                component={Pantry}
            />
            <Stack.Screen
                name="ItemInfo"
                component={ItemInfo}
            />
             <Stack.Screen
                name="EditItemInfo"
                component={EditItemInfo}
            />
        </Stack.Navigator>
    )
}

export default PantryScreen;
