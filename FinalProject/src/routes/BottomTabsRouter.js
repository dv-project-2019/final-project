import React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import FeatherIcon from 'react-native-vector-icons/Feather';
import PantryScreen from './PantryRouter';
import MealPlanScreen from './MealPlanRouter';
import AddItemScreen from './AddItemRouter';
import HistoryScreen from './HistoryRouter';
import MoreScreen from './MoreRouter';

const Tab = createBottomTabNavigator();

const BottomTabsNavigation = () => {
    return (
        <Tab.Navigator
            screenOptions={({ route }) => ({
                tabBarIcon: ({ color, size }) => {
                    if (route.name === 'PantryScreen') {
                        return <MaterialIcon name='kitchen' size={size} color={color} />;
                    } else if (route.name === 'MealPlanScreen') {
                        return <FeatherIcon name='book-open' size={size} color={color} />;
                    } else if (route.name === 'AddItemScreen') {
                        return <FeatherIcon name='plus-circle' size={size} color={color} />;
                    }else if (route.name === 'HistoryScreen') {
                        return <FeatherIcon name='clock' size={size} color={color} />;
                    }else{
                        return <FeatherIcon name='menu' size={size} color={color} />;
                    }
                }
            })}
            tabBarOptions={{
                activeTintColor: '#3D55B6',
                inactiveTintColor: 'gray',
                keyboardHidesTabBar: true
            }}>
            <Tab.Screen name="PantryScreen" component={PantryScreen} options={{ title: 'Pantry' }} />
            <Tab.Screen name="MealPlanScreen" component={MealPlanScreen} options={{ title: 'Meal Plan' }} />
            <Tab.Screen name="AddItemScreen" component={AddItemScreen} options={{ title: 'Add Item' }} />
            <Tab.Screen name="HistoryScreen" component={HistoryScreen} options={{ title: 'History' }} />
            <Tab.Screen name="MoreScreen" component={MoreScreen} options={{ title: 'More' }} />
        </Tab.Navigator>
    );
}

export default BottomTabsNavigation;