import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import History from '../screens/History';

const Stack = createStackNavigator();

const HistoryScreen = () => {
    return (
        <Stack.Navigator
            initialRouteName="History"
            screenOptions={{
                headerShown: false
            }}
        >
            <Stack.Screen
                name="History"
                component={History}
            />
        </Stack.Navigator>
    )
}

export default HistoryScreen;