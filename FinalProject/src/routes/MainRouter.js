import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import BottomTabsNavigation from './BottomTabsRouter';
import MainScreen from '../screens/MainScreen';
import Login from '../screens/Login';
import Register from '../screens/Register';
import { connect } from 'react-redux';

const Stack = createStackNavigator();

const MainRouter = ({ token }) => {
    return (
        <NavigationContainer>
            {token ?
                <Stack.Navigator
                    initialRouteName="BottomTabsNavigation"
                    screenOptions={{
                        headerShown: false
                    }}
                >
                    <Stack.Screen
                        name="BottomTabsNavigation"
                        component={BottomTabsNavigation}
                    />
                </Stack.Navigator>
                :
                <Stack.Navigator
                    initialRouteName="MainScreen"
                    screenOptions={{
                        headerShown: false
                    }}
                >
                    <Stack.Screen
                        name="MainScreen"
                        component={MainScreen}
                    />
                    <Stack.Screen
                        name="Login"
                        component={Login}
                    />
                    <Stack.Screen
                        name="Register"
                        component={Register}
                    />
                    {token ?
                        <Stack.Screen
                            name="BottomTabsNavigation"
                            component={BottomTabsNavigation}
                        />
                        :
                        null
                    }
                </Stack.Navigator>
            }


        </NavigationContainer>
    );
}

const mapStateToProps = (state) => {
    return {
        token: state.user.token
    }
}

export default connect(mapStateToProps, null)(MainRouter);
