import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import More from '../screens/More';
import Profile from '../screens/Profile';
import EditProfile from '../screens/EditProfile';

const Stack = createStackNavigator();

const MoreScreen = () => {
    return (
        <Stack.Navigator
            initialRouteName="More"
            screenOptions={{
                headerShown: false
            }}
        >
            <Stack.Screen
                name="More"
                component={More}
            />
             <Stack.Screen
                name="Profile"
                component={Profile}
            />
            <Stack.Screen
                name="EditProfile"
                component={EditProfile}
            />
        </Stack.Navigator>
    )
}

export default MoreScreen;
