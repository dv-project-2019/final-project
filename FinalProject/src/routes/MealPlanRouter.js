import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import MealPlan from '../screens/MealPlan';
import MealPlanSchedule from '../screens/MealPlanSchedule';

const Stack = createStackNavigator();

const MealPlanScreen = () => {
    return (
        <Stack.Navigator
            initialRouteName="MealPlan"
            screenOptions={{
                headerShown: false
            }}
        >
            <Stack.Screen
                name="MealPlan"
                component={MealPlan}
            />
            <Stack.Screen
                name="AddMealPlanSchedule"
                component={MealPlanSchedule}
            />
        </Stack.Navigator>
    )
}

export default MealPlanScreen;