import { SET_MEALPLAN_ID } from "../actions/mealplanActons";

const initialMealplan = {
    id: ""
};

export const mealplanReducer = (state = initialMealplan, action) => {
    switch (action.type) {
        case SET_MEALPLAN_ID:
            return {
                ...state,
                id: action.mealplanId
            }
        default:
            return state;
    }
}