import { SET_TOKEN, SET_USER_INFO } from "../actions/userAction";

const initialUserInfo = {
    token: "",
    userInfo: {}
};

export const userReducer = (state = initialUserInfo, action) => {
    switch (action.type) {
        case SET_TOKEN:
            return {
                ...state,
                token: action.token
            }
        case SET_USER_INFO:
            return {
                ...state,
                userInfo: action.userInfo
            }
        default:
            return state;
    }
}