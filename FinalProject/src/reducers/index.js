import { combineReducers } from 'redux';
import { mealplanReducer } from './mealplanReducer';
import { userReducer } from './userReducer';

export const rootReducer = combineReducers({
    mealplan: mealplanReducer,
    user: userReducer
})