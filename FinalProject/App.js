import React from 'react';
import { DefaultTheme, Provider as PaperProvider } from 'react-native-paper';
import MainRouter from './src/routes/MainRouter';
import { rootReducer } from './src/reducers';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import AsyncStorage from '@react-native-community/async-storage'
import { PersistGate } from 'redux-persist/integration/react';
import { persistStore, persistReducer } from 'redux-persist';

const persistConfig = {
  key: 'root',
  storage: AsyncStorage
}

const theme = {
  ...DefaultTheme,
  roundness: 2,
  colors: {
    ...DefaultTheme.colors,
    primary: '#4B80D4',
    disabled: '#E1E6F2',
    text: '#2B2B2B'
  },
};

const persistedReducer = persistReducer(persistConfig, rootReducer);
const appStore = createStore(persistedReducer);
const persistor = persistStore(appStore);

const App = () => {
  return (
    <Provider store={appStore}>
      <PersistGate loading={null} persistor={persistor}>
        <PaperProvider theme={theme}>
          <MainRouter />
        </PaperProvider>
      </PersistGate>
    </Provider>
  );
};

export default App;
